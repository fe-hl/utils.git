import { patternNumber } from './regularExp';
import { BrowserType, Icallback, ITHOUSANDTHS } from './types';

// 千分位
export const thousandths = ({
	num,
	decimals = 2,
	decimal = '.',
	separator = ',',
	prefix = '',
	suffix = '',
}: ITHOUSANDTHS = {}): string => {
	if ((num as number) <= 0) {
		if (decimals > 0) {
			return '0.' + new Array(decimals).fill(0).join('');
		}
		return '0';
	}
	num = Number(num).toFixed(decimals);
	num += '';
	const x = num.split('.');
	let x1 = x[0];
	const x2 = x.length > 1 ? decimal + x[1] : '';
	const rgx = /(\d+)(\d{3})/;
	if (separator && isNaN(parseFloat(separator))) {
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + separator + '$2');
		}
	}
	return prefix + x1 + x2 + suffix;
};

// 手机号输入格式化
export const mobileFormat = (mobileStr: string): string | undefined => {
	const inputMobile = mobileStr.replace(/\s|\D/gi, '');
	return /(\d{3})?(\d{1,4})?(\d{1,4})?/
		.exec(inputMobile)
		?.slice(1, 4)
		.join(' ')
		.trim();
};

// 身份证输入格式化
export const cardFormat = (cardStr: string): string | undefined => {
	const inputCard = cardStr.replace(/\s|\D/gi, '');
	const cardNo = /(\d{6})?(\d{1,8})?(\d{1,4})?/
		.exec(inputCard)
		?.slice(1, 4)
		.join(' ')
		.trim();
	const x = cardStr[19];
	return ['x', 'X'].includes(x) ? `${cardNo}${x}` : cardNo;
};

// 银行卡输入格式化
export const bankCardFormat = (bankCardStr: string): string | undefined => {
	const inputBankCardStr = bankCardStr.replace(/\s|\D/gi, '');
	return /(\d{4})?(\d{1,4})?(\d{1,4})?(\d{1,4})?(\d{1,4})?/
		.exec(inputBankCardStr)
		?.slice(1, 6)
		.join(' ')
		.trim();
};

// 远程加载script脚本
export const loadScript = (url: string, callback: Icallback) => {
	const script = document.createElement('script');
	script.onload = () => {
		callback && callback();
	};
	script.src = url;
	document.body.appendChild(script);
};

// 获取url参数
export const queryUrlParams = <T = any>(url: string = location.href): T => {
	const res: any = {};
	url.replace(/([^?=&#]+)=([^?=&#]+)/g, (params: any): any => {
		const arr = params.split('=');
		res[arr[0]] = arr[1];
	});
	return res as T;
};

// 非数字替换为空
export const noNumericToBlank = (value: string): string =>
	value.replace(patternNumber, '');

//获取browser类型
export const getBrowser = (): BrowserType => {
	const u = navigator.userAgent;
	return {
		trident: u.indexOf('Trident') > -1, //IE内核
		presto: u.indexOf('Presto') > -1, //opera内核
		webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
		gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, //火狐内核
		mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
		ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
		android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
		iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
		iPad: u.indexOf('iPad') > -1, //是否iPad
		webApp: u.indexOf('Safari') === -1, //是否web应该程序，没有头部与底部
		qq: u.match(/QQ\/[0-9]/i) ? true : false, // 是否QQ
		qqBrowser: u.indexOf('QQBrowser') > -1, // 是否QQ浏览器
		ali: u.indexOf('AlipayClient') > -1, // 是否支付宝
		weixin: u.indexOf('MicroMessenger') > -1, //是否微信
		yys: u.indexOf('UnionPay') > -1, //是否云闪付
	};
};

export const colorRGBToHex = (rgb: string) => {
	const rgbArr: any = rgb.split(/[^\d]+/);
	const color = (rgbArr[1] << 16) | (rgbArr[2] << 8) | rgbArr[3];
	return '#' + color.toString(16);
};

export const colorHexToRGB = (hex: string) => {
	const newHex: any = hex.replace('#', '0x') as unknown,
		r = newHex >> 16,
		g = (newHex >> 8) & 0xff,
		b = newHex & 0xff;
	return 'rgb(' + r + ',' + g + ',' + b + ')';
};
