export type FormatType = 'YYYY-MM-DD' | 'YYYY-MM-DD HH:mm:ss' | 'HH:mm:ss';

export type DateType = Date | number;

export enum STORAGE {
	LOCAL = 'localStorage',
	SESSION = 'sessionStorage',
}

export interface ITHOUSANDTHS {
	num?: number | string; // 需要转换的数字
	decimals?: number; // 保留几位小数
	decimal?: string; // 十进制分割
	separator?: string; // 千分位分割符
	prefix?: string; // 前缀
	suffix?: string; // 后缀
}

export interface Icallback {
	(): void;
}

export type BrowserType = {
	trident: boolean; //IE内核
	presto: boolean; //opera内核
	webKit: boolean; //苹果、谷歌内核
	gecko: boolean; //火狐内核
	mobile: boolean; //是否为移动终端
	ios: boolean; //ios终端
	android: boolean; //android终端或uc浏览器
	iPhone: boolean; //是否为iPhone或者QQHD浏览器
	iPad: boolean; //是否iPad
	webApp: boolean; //是否web应该程序，没有头部与底部
	qq: boolean; // 是否QQ
	qqBrowser: boolean; // 是否QQ浏览器
	ali: boolean; // 是否支付宝
	weixin: boolean; //是否微信
	yys: boolean; //是否云闪付
};
