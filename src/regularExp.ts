// 检验手机号码
export const patternMobile = /^(1[3-9])\d{9}$/i;
export const isMobile = (value: string): boolean => {
	return patternMobile.test(value);
};

// 检验短信
export const patternSMs = (len = 6): RegExp => new RegExp(`^\\d{${len}}$`);
export const isSMs = (value: string, len = 6): boolean => {
	return patternSMs(len).test(value);
};

// 非纯数字
export const patternNumber = /\D+/g;
export const isNumber = (value: string): boolean => {
	return patternNumber.test(value);
};

// 检验身份证号
export const patternCard = /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{2})(\d)(\d|X|x)$/;
export const isCard = (value: string): boolean => {
	return patternCard.test(value);
};

// 检验邮箱
export const patternEmail = /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/;
export const isEmail = (value: string): boolean => {
	return patternEmail.test(value);
};

// 检验座机
export const patternPhone = /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/;
export const isPhone = (value: string): boolean => {
	return patternPhone.test(value);
};

// 检验URl
export const patternURl =
	/^(?:(http|https|ftp):\/\/)?((?:[\w]+\.)+[a-z0-9]+)((?:\/[^/?#]*)+)?(\?[^#]+)?(#.+)?$/i;
export const isURl = (value: string): boolean => {
	return patternURl.test(value);
};

// 密码以字母开头，长度默认在6~18之间，只能包含字母、数字和下划线
export const patternPwd = (beginLen = 5, endLen = 17): RegExp =>
	new RegExp(`^[a-zA-Z]\\w{${beginLen},${endLen}}$`);
export const isPwd = (value: string, beginLen = 5, endLen = 17): boolean => {
	return patternPwd(beginLen, endLen).test(value);
};

// 检验金额(默认小数点1到2位)
export const patternMoney = (beginLen = 1, endLen = 2): RegExp =>
	new RegExp(`^\\d+(\\.\\d{${beginLen},${endLen}})?$`);
export const isMoney = (value: string, beginLen = 1, endLen = 2): boolean => {
	return patternMoney(beginLen, endLen).test(value);
};

// 检验中文(默认1到2位)
export const patternChinese = (beginLen = 1, endLen = 2): RegExp =>
	new RegExp(`^[\\u4E00-\\u9FA5]{${beginLen},${endLen}}$`);

export const isChinese = (value: string, beginLen = 1, endLen = 2): boolean => {
	return patternChinese(beginLen, endLen).test(value);
};
