import dayjs from 'dayjs';
import { DateType, FormatType } from './types';

// 日期格式化
export const dateFormat = (
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).format(format);
};

// 日期时间戳
export const dateToTime = (d: Date): number => dayjs(d).valueOf();

// 加天数
export const dateAddDay = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).add(n, 'day').format(format);
};

// 减天数
export const dateSubtractDay = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).subtract(n, 'day').format(format);
};

// 加月份
export const dateAddMonth = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).add(n, 'month').format(format);
};

// 减月份
export const dateSubtractMonth = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).subtract(n, 'month').format(format);
};

// 加年份
export const dateAddYears = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).add(n, 'years').format(format);
};

// 减年份
export const dateSubtractYears = (
	n: number,
	d: DateType = new Date(),
	format: FormatType = 'YYYY-MM-DD'
): string => {
	return dayjs(d).subtract(n, 'years').format(format);
};

// 年差
export const dateDiffYears = (
	diffDate: DateType,
	d: DateType = new Date()
): number => {
	return dayjs(d).diff(dayjs(diffDate), 'years');
};

// 月差
export const dateDiffMonth = (
	diffDate: DateType,
	d: DateType = new Date()
): number => {
	return dayjs(d).diff(dayjs(diffDate), 'month');
};

// 天差
export const dateDiffDay = (
	diffDate: DateType,
	d: DateType = new Date()
): number => {
	return dayjs(d).diff(dayjs(diffDate), 'day');
};

// 时差
export const dateDiffHour = (
	diffDate: DateType,
	d: DateType = new Date()
): number => {
	return dayjs(d).diff(dayjs(diffDate), 'hour');
};
