# 使用说明

> javascript 常用工具库，支持 typescript

::: tip
可以直接在控制台调试，函数绑定在 window 上
:::

## npm 安装

```sh
npm i @fe-hl/shared -s
```

### 导入方式一

```js
import * as feHl from '@fe-hl/shared';
```

### 导入方式二

```js
import {
	dateAddDay,
	dateAddMonth,
	dateAddYears,
	dateDiffDay,
	dateDiffHour,
	dateDiffMonth,
	dateDiffYears,
	dateFormat,
	dateSubtractDay,
	dateSubtractMonth,
	dateSubtractYears,
	dateToTime,
	storageGet,
	storageSet,
	storageDeleteKey,
	...
} from '@fe-hl/shared';
```
