# 工具函数

## 函数引用

```js
import {
	thousandths,
	mobileFormat,
	cardFormat,
	bankCardFormat,
	loadScript,
	queryUrlParams,
	noNumericToBlank,
	getBrowser,
	colorHexToRGB,
	colorRGBToHex
} from '@fe-hl/shared';
```

## 函数调用参数说明

```js
export interface ITHOUSANDTHS {
    num?: number | string;
    decimals?: number;
    decimal?: string;
    separator?: string;
    prefix?: string;
    suffix?: string;
}
export interface Icallback {
    (): void;
}
export declare type BrowserType = {
    trident: boolean;
    presto: boolean;
    webKit: boolean;
    gecko: boolean;
    mobile: boolean;
    ios: boolean;
    android: boolean;
    iPhone: boolean;
    iPad: boolean;
    webApp: boolean;
    qq: boolean;
    qqBrowser: boolean;
    ali: boolean;
    weixin: boolean;
    yys: boolean;
}
const thousandths: ({ num, decimals, decimal, separator, prefix, suffix, }?: ITHOUSANDTHS) => string;
const mobileFormat: (mobileStr: string) => string | undefined;
const cardFormat: (cardStr: string) => string | undefined;
const bankCardFormat: (bankCardStr: string) => string | undefined;
const loadScript: (url: string, callback: Icallback) => void;
const queryUrlParams: <T = any>(url?: string) => T;
const noNumericToBlank: (value: string) => string;
const getBrowser: () => BrowserType;
const colorRGBToHex: (rgb:string) => string;
const colorHexToRGB: (rgb:hex) => string;
```

## thousandths 千分位

- `num` 金额
- `decimals` 小数位 默认 `[2]` 位
- `decimal` 小数位符号 默认 `[.]`
- `separator` 千分位分隔符 默认`[,]`
- `prefix` 前缀
- `suffix` 后缀

```js
thousandths({
	num: 10000,
	decimals: 2,
	decimal: '.',
	separator: ',',
	prefix: '',
	suffix: '',
}); // 10,000.00
```

## mobileFormat 手机号码格式化

```js
mobileFormat('13726362333'); // 137 2636 2333
```

## cardFormat 身份证格式化

```js
cardFormat('420883199122338773'); // 420 883 19912233 8773
```

## bankCardFormat 银行卡格式化

```js
bankCardFormat('65446546546546456456'); // 6544 6546 5465 4645 6456
```

## loadScript 加载远程脚本

```js
loadScript(`${location.origin + location.pathname}/vconsole.min.js`, () => {
	new window['VConsole']();
});
```

## 非数字替换为空

```js
noNumericToBlank('543534 gdfg gfg 90'); // 54353490
```

## 非数字替换为空

```js
let {trident,presto,webKit,gecko...}= getBrowser();
//   {
// 	trident: true, //IE内核
// 	presto: true, //opera内核
// 	webKit: true, //苹果、谷歌内核
// 	gecko: true, //火狐内核
// 	mobile: true, //是否为移动终端
// 	ios: true, //ios终端
// 	android: true, //android终端或uc浏览器
// 	iPhone: true, //是否为iPhone或者QQHD浏览器
// 	iPad: true, //是否iPad
// 	webApp: true, //是否web应该程序，没有头部与底部
// 	qq: true, // 是否QQ
// 	qqBrowser: true, // 是否QQ浏览器
// 	ali: true, // 是否支付宝
// 	weixin: true, //是否微信
// 	yys: true, //是否云闪付
// };
```


## rgb转十六进制颜色

```js
colorRGBToHex("rgb(204,0,255)") // #CC00FF
```

## 十六进制颜色转rgb

```js
colorHexToRGB("#CC00FF") // rgb(204,0,255)
```
