// node_modules/@fe-hl/shared/lib/index.esm.js
var t = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
var r = { exports: {} };
r.exports = function() {
  var t3 = 1e3, r2 = 6e4, n2 = 36e5, e2 = "millisecond", i2 = "second", o2 = "minute", u2 = "hour", c2 = "day", a2 = "week", f2 = "month", s2 = "quarter", l2 = "year", h2 = "date", d2 = "Invalid Date", v2 = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/, p2 = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g, g2 = { name: "en", weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_") }, y2 = function(t4, r3, n3) {
    var e3 = String(t4);
    return !e3 || e3.length >= r3 ? t4 : "" + Array(r3 + 1 - e3.length).join(n3) + t4;
  }, m2 = { s: y2, z: function(t4) {
    var r3 = -t4.utcOffset(), n3 = Math.abs(r3), e3 = Math.floor(n3 / 60), i3 = n3 % 60;
    return (r3 <= 0 ? "+" : "-") + y2(e3, 2, "0") + ":" + y2(i3, 2, "0");
  }, m: function t4(r3, n3) {
    if (r3.date() < n3.date())
      return -t4(n3, r3);
    var e3 = 12 * (n3.year() - r3.year()) + (n3.month() - r3.month()), i3 = r3.clone().add(e3, f2), o3 = n3 - i3 < 0, u3 = r3.clone().add(e3 + (o3 ? -1 : 1), f2);
    return +(-(e3 + (n3 - i3) / (o3 ? i3 - u3 : u3 - i3)) || 0);
  }, a: function(t4) {
    return t4 < 0 ? Math.ceil(t4) || 0 : Math.floor(t4);
  }, p: function(t4) {
    return { M: f2, y: l2, w: a2, d: c2, D: h2, h: u2, m: o2, s: i2, ms: e2, Q: s2 }[t4] || String(t4 || "").toLowerCase().replace(/s$/, "");
  }, u: function(t4) {
    return void 0 === t4;
  } }, b2 = "en", w2 = {};
  w2[b2] = g2;
  var x2 = function(t4) {
    return t4 instanceof $2;
  }, S2 = function t4(r3, n3, e3) {
    var i3;
    if (!r3)
      return b2;
    if ("string" == typeof r3) {
      var o3 = r3.toLowerCase();
      w2[o3] && (i3 = o3), n3 && (w2[o3] = n3, i3 = o3);
      var u3 = r3.split("-");
      if (!i3 && u3.length > 1)
        return t4(u3[0]);
    } else {
      var c3 = r3.name;
      w2[c3] = r3, i3 = c3;
    }
    return !e3 && i3 && (b2 = i3), i3 || !e3 && b2;
  }, O2 = function(t4, r3) {
    if (x2(t4))
      return t4.clone();
    var n3 = "object" == typeof r3 ? r3 : {};
    return n3.date = t4, n3.args = arguments, new $2(n3);
  }, E2 = m2;
  E2.l = S2, E2.i = x2, E2.w = function(t4, r3) {
    return O2(t4, { locale: r3.$L, utc: r3.$u, x: r3.$x, $offset: r3.$offset });
  };
  var $2 = function() {
    function g3(t4) {
      this.$L = S2(t4.locale, null, true), this.parse(t4);
    }
    var y3 = g3.prototype;
    return y3.parse = function(t4) {
      this.$d = function(t5) {
        var r3 = t5.date, n3 = t5.utc;
        if (null === r3)
          return /* @__PURE__ */ new Date(NaN);
        if (E2.u(r3))
          return /* @__PURE__ */ new Date();
        if (r3 instanceof Date)
          return new Date(r3);
        if ("string" == typeof r3 && !/Z$/i.test(r3)) {
          var e3 = r3.match(v2);
          if (e3) {
            var i3 = e3[2] - 1 || 0, o3 = (e3[7] || "0").substring(0, 3);
            return n3 ? new Date(Date.UTC(e3[1], i3, e3[3] || 1, e3[4] || 0, e3[5] || 0, e3[6] || 0, o3)) : new Date(e3[1], i3, e3[3] || 1, e3[4] || 0, e3[5] || 0, e3[6] || 0, o3);
          }
        }
        return new Date(r3);
      }(t4), this.$x = t4.x || {}, this.init();
    }, y3.init = function() {
      var t4 = this.$d;
      this.$y = t4.getFullYear(), this.$M = t4.getMonth(), this.$D = t4.getDate(), this.$W = t4.getDay(), this.$H = t4.getHours(), this.$m = t4.getMinutes(), this.$s = t4.getSeconds(), this.$ms = t4.getMilliseconds();
    }, y3.$utils = function() {
      return E2;
    }, y3.isValid = function() {
      return !(this.$d.toString() === d2);
    }, y3.isSame = function(t4, r3) {
      var n3 = O2(t4);
      return this.startOf(r3) <= n3 && n3 <= this.endOf(r3);
    }, y3.isAfter = function(t4, r3) {
      return O2(t4) < this.startOf(r3);
    }, y3.isBefore = function(t4, r3) {
      return this.endOf(r3) < O2(t4);
    }, y3.$g = function(t4, r3, n3) {
      return E2.u(t4) ? this[r3] : this.set(n3, t4);
    }, y3.unix = function() {
      return Math.floor(this.valueOf() / 1e3);
    }, y3.valueOf = function() {
      return this.$d.getTime();
    }, y3.startOf = function(t4, r3) {
      var n3 = this, e3 = !!E2.u(r3) || r3, s3 = E2.p(t4), d3 = function(t5, r4) {
        var i3 = E2.w(n3.$u ? Date.UTC(n3.$y, r4, t5) : new Date(n3.$y, r4, t5), n3);
        return e3 ? i3 : i3.endOf(c2);
      }, v3 = function(t5, r4) {
        return E2.w(n3.toDate()[t5].apply(n3.toDate("s"), (e3 ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(r4)), n3);
      }, p3 = this.$W, g4 = this.$M, y4 = this.$D, m3 = "set" + (this.$u ? "UTC" : "");
      switch (s3) {
        case l2:
          return e3 ? d3(1, 0) : d3(31, 11);
        case f2:
          return e3 ? d3(1, g4) : d3(0, g4 + 1);
        case a2:
          var b3 = this.$locale().weekStart || 0, w3 = (p3 < b3 ? p3 + 7 : p3) - b3;
          return d3(e3 ? y4 - w3 : y4 + (6 - w3), g4);
        case c2:
        case h2:
          return v3(m3 + "Hours", 0);
        case u2:
          return v3(m3 + "Minutes", 1);
        case o2:
          return v3(m3 + "Seconds", 2);
        case i2:
          return v3(m3 + "Milliseconds", 3);
        default:
          return this.clone();
      }
    }, y3.endOf = function(t4) {
      return this.startOf(t4, false);
    }, y3.$set = function(t4, r3) {
      var n3, a3 = E2.p(t4), s3 = "set" + (this.$u ? "UTC" : ""), d3 = (n3 = {}, n3[c2] = s3 + "Date", n3[h2] = s3 + "Date", n3[f2] = s3 + "Month", n3[l2] = s3 + "FullYear", n3[u2] = s3 + "Hours", n3[o2] = s3 + "Minutes", n3[i2] = s3 + "Seconds", n3[e2] = s3 + "Milliseconds", n3)[a3], v3 = a3 === c2 ? this.$D + (r3 - this.$W) : r3;
      if (a3 === f2 || a3 === l2) {
        var p3 = this.clone().set(h2, 1);
        p3.$d[d3](v3), p3.init(), this.$d = p3.set(h2, Math.min(this.$D, p3.daysInMonth())).$d;
      } else
        d3 && this.$d[d3](v3);
      return this.init(), this;
    }, y3.set = function(t4, r3) {
      return this.clone().$set(t4, r3);
    }, y3.get = function(t4) {
      return this[E2.p(t4)]();
    }, y3.add = function(e3, s3) {
      var h3, d3 = this;
      e3 = Number(e3);
      var v3 = E2.p(s3), p3 = function(t4) {
        var r3 = O2(d3);
        return E2.w(r3.date(r3.date() + Math.round(t4 * e3)), d3);
      };
      if (v3 === f2)
        return this.set(f2, this.$M + e3);
      if (v3 === l2)
        return this.set(l2, this.$y + e3);
      if (v3 === c2)
        return p3(1);
      if (v3 === a2)
        return p3(7);
      var g4 = (h3 = {}, h3[o2] = r2, h3[u2] = n2, h3[i2] = t3, h3)[v3] || 1, y4 = this.$d.getTime() + e3 * g4;
      return E2.w(y4, this);
    }, y3.subtract = function(t4, r3) {
      return this.add(-1 * t4, r3);
    }, y3.format = function(t4) {
      var r3 = this, n3 = this.$locale();
      if (!this.isValid())
        return n3.invalidDate || d2;
      var e3 = t4 || "YYYY-MM-DDTHH:mm:ssZ", i3 = E2.z(this), o3 = this.$H, u3 = this.$m, c3 = this.$M, a3 = n3.weekdays, f3 = n3.months, s3 = function(t5, n4, i4, o4) {
        return t5 && (t5[n4] || t5(r3, e3)) || i4[n4].slice(0, o4);
      }, l3 = function(t5) {
        return E2.s(o3 % 12 || 12, t5, "0");
      }, h3 = n3.meridiem || function(t5, r4, n4) {
        var e4 = t5 < 12 ? "AM" : "PM";
        return n4 ? e4.toLowerCase() : e4;
      }, v3 = { YY: String(this.$y).slice(-2), YYYY: this.$y, M: c3 + 1, MM: E2.s(c3 + 1, 2, "0"), MMM: s3(n3.monthsShort, c3, f3, 3), MMMM: s3(f3, c3), D: this.$D, DD: E2.s(this.$D, 2, "0"), d: String(this.$W), dd: s3(n3.weekdaysMin, this.$W, a3, 2), ddd: s3(n3.weekdaysShort, this.$W, a3, 3), dddd: a3[this.$W], H: String(o3), HH: E2.s(o3, 2, "0"), h: l3(1), hh: l3(2), a: h3(o3, u3, true), A: h3(o3, u3, false), m: String(u3), mm: E2.s(u3, 2, "0"), s: String(this.$s), ss: E2.s(this.$s, 2, "0"), SSS: E2.s(this.$ms, 3, "0"), Z: i3 };
      return e3.replace(p2, function(t5, r4) {
        return r4 || v3[t5] || i3.replace(":", "");
      });
    }, y3.utcOffset = function() {
      return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
    }, y3.diff = function(e3, h3, d3) {
      var v3, p3 = E2.p(h3), g4 = O2(e3), y4 = (g4.utcOffset() - this.utcOffset()) * r2, m3 = this - g4, b3 = E2.m(this, g4);
      return b3 = (v3 = {}, v3[l2] = b3 / 12, v3[f2] = b3, v3[s2] = b3 / 3, v3[a2] = (m3 - y4) / 6048e5, v3[c2] = (m3 - y4) / 864e5, v3[u2] = m3 / n2, v3[o2] = m3 / r2, v3[i2] = m3 / t3, v3)[p3] || m3, d3 ? b3 : E2.a(b3);
    }, y3.daysInMonth = function() {
      return this.endOf(f2).$D;
    }, y3.$locale = function() {
      return w2[this.$L];
    }, y3.locale = function(t4, r3) {
      if (!t4)
        return this.$L;
      var n3 = this.clone(), e3 = S2(t4, r3, true);
      return e3 && (n3.$L = e3), n3;
    }, y3.clone = function() {
      return E2.w(this.$d, this);
    }, y3.toDate = function() {
      return new Date(this.valueOf());
    }, y3.toJSON = function() {
      return this.isValid() ? this.toISOString() : null;
    }, y3.toISOString = function() {
      return this.$d.toISOString();
    }, y3.toString = function() {
      return this.$d.toUTCString();
    }, g3;
  }(), M2 = $2.prototype;
  return O2.prototype = M2, [["$ms", e2], ["$s", i2], ["$m", o2], ["$H", u2], ["$W", c2], ["$M", f2], ["$y", l2], ["$D", h2]].forEach(function(t4) {
    M2[t4[1]] = function(r3) {
      return this.$g(r3, t4[0], t4[1]);
    };
  }), O2.extend = function(t4, r3) {
    return t4.$i || (t4(r3, $2, O2), t4.$i = true), O2;
  }, O2.locale = S2, O2.isDayjs = x2, O2.unix = function(t4) {
    return O2(1e3 * t4);
  }, O2.en = w2[b2], O2.Ls = w2, O2.p = {}, O2;
}();
var n = r.exports;
var e = function() {
  var t3 = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : /* @__PURE__ */ new Date(), r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "YYYY-MM-DD";
  return n(t3).format(r2);
};
var i = function(t3) {
  return n(t3).valueOf();
};
var o = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).add(t3, "day").format(e2);
};
var u = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).subtract(t3, "day").format(e2);
};
var c = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).add(t3, "month").format(e2);
};
var a = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).subtract(t3, "month").format(e2);
};
var f = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).add(t3, "years").format(e2);
};
var s = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date(), e2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "YYYY-MM-DD";
  return n(r2).subtract(t3, "years").format(e2);
};
var l = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date();
  return n(r2).diff(n(t3), "years");
};
var h = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date();
  return n(r2).diff(n(t3), "month");
};
var d = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date();
  return n(r2).diff(n(t3), "day");
};
var v = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : /* @__PURE__ */ new Date();
  return n(r2).diff(n(t3), "hour");
};
var p = function(t3) {
  return t3 && t3.Math == Math && t3;
};
var g = p("object" == typeof globalThis && globalThis) || p("object" == typeof window && window) || p("object" == typeof self && self) || p("object" == typeof t && t) || function() {
  return this;
}() || Function("return this")();
var y = {};
var m = function(t3) {
  try {
    return !!t3();
  } catch (t4) {
    return true;
  }
};
var b = !m(function() {
  return 7 != Object.defineProperty({}, 1, { get: function() {
    return 7;
  } })[1];
});
var w = !m(function() {
  var t3 = function() {
  }.bind();
  return "function" != typeof t3 || t3.hasOwnProperty("prototype");
});
var x = w;
var S = Function.prototype.call;
var O = x ? S.bind(S) : function() {
  return S.apply(S, arguments);
};
var E = {};
var $ = {}.propertyIsEnumerable;
var M = Object.getOwnPropertyDescriptor;
var D = M && !$.call({ 1: 2 }, 1);
E.f = D ? function(t3) {
  var r2 = M(this, t3);
  return !!r2 && r2.enumerable;
} : $;
var I;
var A;
var j = function(t3, r2) {
  return { enumerable: !(1 & t3), configurable: !(2 & t3), writable: !(4 & t3), value: r2 };
};
var R = w;
var N = Function.prototype;
var P = N.bind;
var _ = N.call;
var T = R && P.bind(_, _);
var Y = R ? function(t3) {
  return t3 && T(t3);
} : function(t3) {
  return t3 && function() {
    return _.apply(t3, arguments);
  };
};
var F = Y;
var C = F({}.toString);
var k = F("".slice);
var L = function(t3) {
  return k(C(t3), 8, -1);
};
var U = m;
var W = L;
var H = Object;
var q = Y("".split);
var z = U(function() {
  return !H("z").propertyIsEnumerable(0);
}) ? function(t3) {
  return "String" == W(t3) ? q(t3, "") : H(t3);
} : H;
var G = function(t3) {
  return null == t3;
};
var K = G;
var B = TypeError;
var J = function(t3) {
  if (K(t3))
    throw B("Can't call method on " + t3);
  return t3;
};
var V = z;
var X = J;
var Q = function(t3) {
  return V(X(t3));
};
var Z = function(t3) {
  return "function" == typeof t3;
};
var tt = Z;
var rt = "object" == typeof document && document.all;
var nt = void 0 === rt && void 0 !== rt ? function(t3) {
  return "object" == typeof t3 ? null !== t3 : tt(t3) || t3 === rt;
} : function(t3) {
  return "object" == typeof t3 ? null !== t3 : tt(t3);
};
var et = g;
var it = Z;
var ot = function(t3, r2) {
  return arguments.length < 2 ? (n2 = et[t3], it(n2) ? n2 : void 0) : et[t3] && et[t3][r2];
  var n2;
};
var ut = Y({}.isPrototypeOf);
var ct = g;
var at = ot("navigator", "userAgent") || "";
var ft = ct.process;
var st = ct.Deno;
var lt = ft && ft.versions || st && st.version;
var ht = lt && lt.v8;
ht && (A = (I = ht.split("."))[0] > 0 && I[0] < 4 ? 1 : +(I[0] + I[1])), !A && at && (!(I = at.match(/Edge\/(\d+)/)) || I[1] >= 74) && (I = at.match(/Chrome\/(\d+)/)) && (A = +I[1]);
var dt = A;
var vt = dt;
var pt = m;
var gt = !!Object.getOwnPropertySymbols && !pt(function() {
  var t3 = Symbol();
  return !String(t3) || !(Object(t3) instanceof Symbol) || !Symbol.sham && vt && vt < 41;
});
var yt = gt && !Symbol.sham && "symbol" == typeof Symbol.iterator;
var mt = ot;
var bt = Z;
var wt = ut;
var xt = Object;
var St = yt ? function(t3) {
  return "symbol" == typeof t3;
} : function(t3) {
  var r2 = mt("Symbol");
  return bt(r2) && wt(r2.prototype, xt(t3));
};
var Ot = String;
var Et = function(t3) {
  try {
    return Ot(t3);
  } catch (t4) {
    return "Object";
  }
};
var $t = Z;
var Mt = Et;
var Dt = TypeError;
var It = function(t3) {
  if ($t(t3))
    return t3;
  throw Dt(Mt(t3) + " is not a function");
};
var At = G;
var jt = function(t3, r2) {
  var n2 = t3[r2];
  return At(n2) ? void 0 : It(n2);
};
var Rt = O;
var Nt = Z;
var Pt = nt;
var _t = TypeError;
var Tt = { exports: {} };
var Yt = g;
var Ft = Object.defineProperty;
var Ct = function(t3, r2) {
  try {
    Ft(Yt, t3, { value: r2, configurable: true, writable: true });
  } catch (n2) {
    Yt[t3] = r2;
  }
  return r2;
};
var kt = Ct;
var Lt = "__core-js_shared__";
var Ut = g[Lt] || kt(Lt, {});
var Wt = Ut;
(Tt.exports = function(t3, r2) {
  return Wt[t3] || (Wt[t3] = void 0 !== r2 ? r2 : {});
})("versions", []).push({ version: "3.25.0", mode: "global", copyright: "© 2014-2022 Denis Pushkarev (zloirock.ru)", license: "https://github.com/zloirock/core-js/blob/v3.25.0/LICENSE", source: "https://github.com/zloirock/core-js" });
var Ht = J;
var qt = Object;
var zt = function(t3) {
  return qt(Ht(t3));
};
var Gt = zt;
var Kt = Y({}.hasOwnProperty);
var Bt = Object.hasOwn || function(t3, r2) {
  return Kt(Gt(t3), r2);
};
var Jt = Y;
var Vt = 0;
var Xt = Math.random();
var Qt = Jt(1 .toString);
var Zt = function(t3) {
  return "Symbol(" + (void 0 === t3 ? "" : t3) + ")_" + Qt(++Vt + Xt, 36);
};
var tr = g;
var rr = Tt.exports;
var nr = Bt;
var er = Zt;
var ir = gt;
var or = yt;
var ur = rr("wks");
var cr = tr.Symbol;
var ar = cr && cr.for;
var fr = or ? cr : cr && cr.withoutSetter || er;
var sr = function(t3) {
  if (!nr(ur, t3) || !ir && "string" != typeof ur[t3]) {
    var r2 = "Symbol." + t3;
    ir && nr(cr, t3) ? ur[t3] = cr[t3] : ur[t3] = or && ar ? ar(r2) : fr(r2);
  }
  return ur[t3];
};
var lr = O;
var hr = nt;
var dr = St;
var vr = jt;
var pr = function(t3, r2) {
  var n2, e2;
  if ("string" === r2 && Nt(n2 = t3.toString) && !Pt(e2 = Rt(n2, t3)))
    return e2;
  if (Nt(n2 = t3.valueOf) && !Pt(e2 = Rt(n2, t3)))
    return e2;
  if ("string" !== r2 && Nt(n2 = t3.toString) && !Pt(e2 = Rt(n2, t3)))
    return e2;
  throw _t("Can't convert object to primitive value");
};
var gr = TypeError;
var yr = sr("toPrimitive");
var mr = function(t3, r2) {
  if (!hr(t3) || dr(t3))
    return t3;
  var n2, e2 = vr(t3, yr);
  if (e2) {
    if (void 0 === r2 && (r2 = "default"), n2 = lr(e2, t3, r2), !hr(n2) || dr(n2))
      return n2;
    throw gr("Can't convert object to primitive value");
  }
  return void 0 === r2 && (r2 = "number"), pr(t3, r2);
};
var br = mr;
var wr = St;
var xr = function(t3) {
  var r2 = br(t3, "string");
  return wr(r2) ? r2 : r2 + "";
};
var Sr = nt;
var Or = g.document;
var Er = Sr(Or) && Sr(Or.createElement);
var $r = function(t3) {
  return Er ? Or.createElement(t3) : {};
};
var Mr = $r;
var Dr = !b && !m(function() {
  return 7 != Object.defineProperty(Mr("div"), "a", { get: function() {
    return 7;
  } }).a;
});
var Ir = b;
var Ar = O;
var jr = E;
var Rr = j;
var Nr = Q;
var Pr = xr;
var _r = Bt;
var Tr = Dr;
var Yr = Object.getOwnPropertyDescriptor;
y.f = Ir ? Yr : function(t3, r2) {
  if (t3 = Nr(t3), r2 = Pr(r2), Tr)
    try {
      return Yr(t3, r2);
    } catch (t4) {
    }
  if (_r(t3, r2))
    return Rr(!Ar(jr.f, t3, r2), t3[r2]);
};
var Fr = {};
var Cr = b && m(function() {
  return 42 != Object.defineProperty(function() {
  }, "prototype", { value: 42, writable: false }).prototype;
});
var kr = nt;
var Lr = String;
var Ur = TypeError;
var Wr = function(t3) {
  if (kr(t3))
    return t3;
  throw Ur(Lr(t3) + " is not an object");
};
var Hr = b;
var qr = Dr;
var zr = Cr;
var Gr = Wr;
var Kr = xr;
var Br = TypeError;
var Jr = Object.defineProperty;
var Vr = Object.getOwnPropertyDescriptor;
var Xr = "enumerable";
var Qr = "configurable";
var Zr = "writable";
Fr.f = Hr ? zr ? function(t3, r2, n2) {
  if (Gr(t3), r2 = Kr(r2), Gr(n2), "function" == typeof t3 && "prototype" === r2 && "value" in n2 && Zr in n2 && !n2[Zr]) {
    var e2 = Vr(t3, r2);
    e2 && e2[Zr] && (t3[r2] = n2.value, n2 = { configurable: Qr in n2 ? n2[Qr] : e2[Qr], enumerable: Xr in n2 ? n2[Xr] : e2[Xr], writable: false });
  }
  return Jr(t3, r2, n2);
} : Jr : function(t3, r2, n2) {
  if (Gr(t3), r2 = Kr(r2), Gr(n2), qr)
    try {
      return Jr(t3, r2, n2);
    } catch (t4) {
    }
  if ("get" in n2 || "set" in n2)
    throw Br("Accessors not supported");
  return "value" in n2 && (t3[r2] = n2.value), t3;
};
var tn = Fr;
var rn = j;
var nn = b ? function(t3, r2, n2) {
  return tn.f(t3, r2, rn(1, n2));
} : function(t3, r2, n2) {
  return t3[r2] = n2, t3;
};
var en = { exports: {} };
var on = b;
var un = Bt;
var cn = Function.prototype;
var an = on && Object.getOwnPropertyDescriptor;
var fn = un(cn, "name");
var sn = { EXISTS: fn, PROPER: fn && "something" === function() {
}.name, CONFIGURABLE: fn && (!on || on && an(cn, "name").configurable) };
var ln = Z;
var hn = Ut;
var dn = Y(Function.toString);
ln(hn.inspectSource) || (hn.inspectSource = function(t3) {
  return dn(t3);
});
var vn;
var pn;
var gn;
var yn = hn.inspectSource;
var mn = Z;
var bn = g.WeakMap;
var wn = mn(bn) && /native code/.test(String(bn));
var xn = Tt.exports;
var Sn = Zt;
var On = xn("keys");
var En = function(t3) {
  return On[t3] || (On[t3] = Sn(t3));
};
var $n = {};
var Mn = wn;
var Dn = g;
var In = Y;
var An = nt;
var jn = nn;
var Rn = Bt;
var Nn = Ut;
var Pn = En;
var _n = $n;
var Tn = "Object already initialized";
var Yn = Dn.TypeError;
var Fn = Dn.WeakMap;
if (Mn || Nn.state) {
  Cn = Nn.state || (Nn.state = new Fn()), kn = In(Cn.get), Ln = In(Cn.has), Un = In(Cn.set);
  vn = function(t3, r2) {
    if (Ln(Cn, t3))
      throw Yn(Tn);
    return r2.facade = t3, Un(Cn, t3, r2), r2;
  }, pn = function(t3) {
    return kn(Cn, t3) || {};
  }, gn = function(t3) {
    return Ln(Cn, t3);
  };
} else {
  Wn = Pn("state");
  _n[Wn] = true, vn = function(t3, r2) {
    if (Rn(t3, Wn))
      throw Yn(Tn);
    return r2.facade = t3, jn(t3, Wn, r2), r2;
  }, pn = function(t3) {
    return Rn(t3, Wn) ? t3[Wn] : {};
  }, gn = function(t3) {
    return Rn(t3, Wn);
  };
}
var Cn;
var kn;
var Ln;
var Un;
var Wn;
var Hn = { set: vn, get: pn, has: gn, enforce: function(t3) {
  return gn(t3) ? pn(t3) : vn(t3, {});
}, getterFor: function(t3) {
  return function(r2) {
    var n2;
    if (!An(r2) || (n2 = pn(r2)).type !== t3)
      throw Yn("Incompatible receiver, " + t3 + " required");
    return n2;
  };
} };
var qn = m;
var zn = Z;
var Gn = Bt;
var Kn = b;
var Bn = sn.CONFIGURABLE;
var Jn = yn;
var Vn = Hn.enforce;
var Xn = Hn.get;
var Qn = Object.defineProperty;
var Zn = Kn && !qn(function() {
  return 8 !== Qn(function() {
  }, "length", { value: 8 }).length;
});
var te = String(String).split("String");
var re = en.exports = function(t3, r2, n2) {
  "Symbol(" === String(r2).slice(0, 7) && (r2 = "[" + String(r2).replace(/^Symbol\(([^)]*)\)/, "$1") + "]"), n2 && n2.getter && (r2 = "get " + r2), n2 && n2.setter && (r2 = "set " + r2), (!Gn(t3, "name") || Bn && t3.name !== r2) && (Kn ? Qn(t3, "name", { value: r2, configurable: true }) : t3.name = r2), Zn && n2 && Gn(n2, "arity") && t3.length !== n2.arity && Qn(t3, "length", { value: n2.arity });
  try {
    n2 && Gn(n2, "constructor") && n2.constructor ? Kn && Qn(t3, "prototype", { writable: false }) : t3.prototype && (t3.prototype = void 0);
  } catch (t4) {
  }
  var e2 = Vn(t3);
  return Gn(e2, "source") || (e2.source = te.join("string" == typeof r2 ? r2 : "")), t3;
};
Function.prototype.toString = re(function() {
  return zn(this) && Xn(this).source || Jn(this);
}, "toString");
var ne = Z;
var ee = Fr;
var ie = en.exports;
var oe = Ct;
var ue = function(t3, r2, n2, e2) {
  e2 || (e2 = {});
  var i2 = e2.enumerable, o2 = void 0 !== e2.name ? e2.name : r2;
  if (ne(n2) && ie(n2, o2, e2), e2.global)
    i2 ? t3[r2] = n2 : oe(r2, n2);
  else {
    try {
      e2.unsafe ? t3[r2] && (i2 = true) : delete t3[r2];
    } catch (t4) {
    }
    i2 ? t3[r2] = n2 : ee.f(t3, r2, { value: n2, enumerable: false, configurable: !e2.nonConfigurable, writable: !e2.nonWritable });
  }
  return t3;
};
var ce = {};
var ae = Math.ceil;
var fe = Math.floor;
var se = Math.trunc || function(t3) {
  var r2 = +t3;
  return (r2 > 0 ? fe : ae)(r2);
};
var le = function(t3) {
  var r2 = +t3;
  return r2 != r2 || 0 === r2 ? 0 : se(r2);
};
var he = le;
var de = Math.max;
var ve = Math.min;
var pe = function(t3, r2) {
  var n2 = he(t3);
  return n2 < 0 ? de(n2 + r2, 0) : ve(n2, r2);
};
var ge = le;
var ye = Math.min;
var me = function(t3) {
  return t3 > 0 ? ye(ge(t3), 9007199254740991) : 0;
};
var be = me;
var we = function(t3) {
  return be(t3.length);
};
var xe = Q;
var Se = pe;
var Oe = we;
var Ee = function(t3) {
  return function(r2, n2, e2) {
    var i2, o2 = xe(r2), u2 = Oe(o2), c2 = Se(e2, u2);
    if (t3 && n2 != n2) {
      for (; u2 > c2; )
        if ((i2 = o2[c2++]) != i2)
          return true;
    } else
      for (; u2 > c2; c2++)
        if ((t3 || c2 in o2) && o2[c2] === n2)
          return t3 || c2 || 0;
    return !t3 && -1;
  };
};
var $e = { includes: Ee(true), indexOf: Ee(false) };
var Me = Bt;
var De = Q;
var Ie = $e.indexOf;
var Ae = $n;
var je = Y([].push);
var Re = function(t3, r2) {
  var n2, e2 = De(t3), i2 = 0, o2 = [];
  for (n2 in e2)
    !Me(Ae, n2) && Me(e2, n2) && je(o2, n2);
  for (; r2.length > i2; )
    Me(e2, n2 = r2[i2++]) && (~Ie(o2, n2) || je(o2, n2));
  return o2;
};
var Ne = ["constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf"];
var Pe = Re;
var _e = Ne.concat("length", "prototype");
ce.f = Object.getOwnPropertyNames || function(t3) {
  return Pe(t3, _e);
};
var Te = {};
Te.f = Object.getOwnPropertySymbols;
var Ye;
var Fe = ot;
var Ce = ce;
var ke = Te;
var Le = Wr;
var Ue = Y([].concat);
var We = Fe("Reflect", "ownKeys") || function(t3) {
  var r2 = Ce.f(Le(t3)), n2 = ke.f;
  return n2 ? Ue(r2, n2(t3)) : r2;
};
var He = Bt;
var qe = We;
var ze = y;
var Ge = Fr;
var Ke = m;
var Be = Z;
var Je = /#|\.prototype\./;
var Ve = function(t3, r2) {
  var n2 = Qe[Xe(t3)];
  return n2 == ti || n2 != Ze && (Be(r2) ? Ke(r2) : !!r2);
};
var Xe = Ve.normalize = function(t3) {
  return String(t3).replace(Je, ".").toLowerCase();
};
var Qe = Ve.data = {};
var Ze = Ve.NATIVE = "N";
var ti = Ve.POLYFILL = "P";
var ri = Ve;
var ni = g;
var ei = y.f;
var ii = nn;
var oi = ue;
var ui = Ct;
var ci = function(t3, r2, n2) {
  for (var e2 = qe(r2), i2 = Ge.f, o2 = ze.f, u2 = 0; u2 < e2.length; u2++) {
    var c2 = e2[u2];
    He(t3, c2) || n2 && He(n2, c2) || i2(t3, c2, o2(r2, c2));
  }
};
var ai = ri;
var fi = function(t3, r2) {
  var n2, e2, i2, o2, u2, c2 = t3.target, a2 = t3.global, f2 = t3.stat;
  if (n2 = a2 ? ni : f2 ? ni[c2] || ui(c2, {}) : (ni[c2] || {}).prototype)
    for (e2 in r2) {
      if (o2 = r2[e2], i2 = t3.dontCallGetSet ? (u2 = ei(n2, e2)) && u2.value : n2[e2], !ai(a2 ? e2 : c2 + (f2 ? "." : "#") + e2, t3.forced) && void 0 !== i2) {
        if (typeof o2 == typeof i2)
          continue;
        ci(o2, i2);
      }
      (t3.sham || i2 && i2.sham) && ii(o2, "sham", true), oi(n2, e2, o2, t3);
    }
};
var si = w;
var li = Function.prototype;
var hi = li.apply;
var di = li.call;
var vi = "object" == typeof Reflect && Reflect.apply || (si ? di.bind(hi) : function() {
  return di.apply(hi, arguments);
});
var pi = L;
var gi = Array.isArray || function(t3) {
  return "Array" == pi(t3);
};
var yi = Y([].slice);
var mi = fi;
var bi = ot;
var wi = vi;
var xi = O;
var Si = Y;
var Oi = m;
var Ei = gi;
var $i = Z;
var Mi = nt;
var Di = St;
var Ii = yi;
var Ai = gt;
var ji = bi("JSON", "stringify");
var Ri = Si(/./.exec);
var Ni = Si("".charAt);
var Pi = Si("".charCodeAt);
var _i = Si("".replace);
var Ti = Si(1 .toString);
var Yi = /[\uD800-\uDFFF]/g;
var Fi = /^[\uD800-\uDBFF]$/;
var Ci = /^[\uDC00-\uDFFF]$/;
var ki = !Ai || Oi(function() {
  var t3 = bi("Symbol")();
  return "[null]" != ji([t3]) || "{}" != ji({ a: t3 }) || "{}" != ji(Object(t3));
});
var Li = Oi(function() {
  return '"\\udf06\\ud834"' !== ji("\uDF06\uD834") || '"\\udead"' !== ji("\uDEAD");
});
var Ui = function(t3, r2) {
  var n2 = Ii(arguments), e2 = r2;
  if ((Mi(r2) || void 0 !== t3) && !Di(t3))
    return Ei(r2) || (r2 = function(t4, r3) {
      if ($i(e2) && (r3 = xi(e2, this, t4, r3)), !Di(r3))
        return r3;
    }), n2[1] = r2, wi(ji, null, n2);
};
var Wi = function(t3, r2, n2) {
  var e2 = Ni(n2, r2 - 1), i2 = Ni(n2, r2 + 1);
  return Ri(Fi, t3) && !Ri(Ci, i2) || Ri(Ci, t3) && !Ri(Fi, e2) ? "\\u" + Ti(Pi(t3, 0), 16) : t3;
};
ji && mi({ target: "JSON", stat: true, arity: 3, forced: ki || Li }, { stringify: function(t3, r2, n2) {
  var e2 = Ii(arguments), i2 = wi(ki ? Ui : ji, null, e2);
  return Li && "string" == typeof i2 ? _i(i2, Yi, Wi) : i2;
} }), function(t3) {
  t3.LOCAL = "localStorage", t3.SESSION = "sessionStorage";
}(Ye || (Ye = {}));
var Hi = "STORAGE_";
var qi = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : Ye.SESSION, n2 = window[r2].getItem("".concat(Hi).concat(t3));
  if (n2)
    try {
      return JSON.parse(n2);
    } catch (t4) {
      return n2;
    }
  return "";
};
var zi = function(t3, r2) {
  var n2 = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : Ye.SESSION;
  r2 instanceof Object && (r2 = JSON.stringify(r2)), window[n2].setItem("".concat(Hi).concat(t3), r2);
};
var Gi = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : Ye.SESSION;
  window[r2].removeItem("".concat(Hi).concat(t3));
};
var Ki = {};
Ki[sr("toStringTag")] = "z";
var Bi = "[object z]" === String(Ki);
var Ji = Bi;
var Vi = Z;
var Xi = L;
var Qi = sr("toStringTag");
var Zi = Object;
var to = "Arguments" == Xi(function() {
  return arguments;
}());
var ro = Ji ? Xi : function(t3) {
  var r2, n2, e2;
  return void 0 === t3 ? "Undefined" : null === t3 ? "Null" : "string" == typeof (n2 = function(t4, r3) {
    try {
      return t4[r3];
    } catch (t5) {
    }
  }(r2 = Zi(t3), Qi)) ? n2 : to ? Xi(r2) : "Object" == (e2 = Xi(r2)) && Vi(r2.callee) ? "Arguments" : e2;
};
var no = ro;
var eo = String;
var io = function(t3) {
  if ("Symbol" === no(t3))
    throw TypeError("Cannot convert a Symbol value to a string");
  return eo(t3);
};
var oo = Wr;
var uo = function() {
  var t3 = oo(this), r2 = "";
  return t3.hasIndices && (r2 += "d"), t3.global && (r2 += "g"), t3.ignoreCase && (r2 += "i"), t3.multiline && (r2 += "m"), t3.dotAll && (r2 += "s"), t3.unicode && (r2 += "u"), t3.unicodeSets && (r2 += "v"), t3.sticky && (r2 += "y"), r2;
};
var co = m;
var ao = g.RegExp;
var fo = co(function() {
  var t3 = ao("a", "y");
  return t3.lastIndex = 2, null != t3.exec("abcd");
});
var so = fo || co(function() {
  return !ao("a", "y").sticky;
});
var lo = fo || co(function() {
  var t3 = ao("^r", "gy");
  return t3.lastIndex = 2, null != t3.exec("str");
});
var ho = { BROKEN_CARET: lo, MISSED_STICKY: so, UNSUPPORTED_Y: fo };
var vo = {};
var po = Re;
var go = Ne;
var yo = Object.keys || function(t3) {
  return po(t3, go);
};
var mo = b;
var bo = Cr;
var wo = Fr;
var xo = Wr;
var So = Q;
var Oo = yo;
vo.f = mo && !bo ? Object.defineProperties : function(t3, r2) {
  xo(t3);
  for (var n2, e2 = So(r2), i2 = Oo(r2), o2 = i2.length, u2 = 0; o2 > u2; )
    wo.f(t3, n2 = i2[u2++], e2[n2]);
  return t3;
};
var Eo;
var $o = ot("document", "documentElement");
var Mo = Wr;
var Do = vo;
var Io = Ne;
var Ao = $n;
var jo = $o;
var Ro = $r;
var No = "prototype";
var Po = "script";
var _o = En("IE_PROTO");
var To = function() {
};
var Yo = function(t3) {
  return "<" + Po + ">" + t3 + "</" + Po + ">";
};
var Fo = function(t3) {
  t3.write(Yo("")), t3.close();
  var r2 = t3.parentWindow.Object;
  return t3 = null, r2;
};
var Co = function() {
  try {
    Eo = new ActiveXObject("htmlfile");
  } catch (t4) {
  }
  var t3, r2, n2;
  Co = "undefined" != typeof document ? document.domain && Eo ? Fo(Eo) : (r2 = Ro("iframe"), n2 = "java" + Po + ":", r2.style.display = "none", jo.appendChild(r2), r2.src = String(n2), (t3 = r2.contentWindow.document).open(), t3.write(Yo("document.F=Object")), t3.close(), t3.F) : Fo(Eo);
  for (var e2 = Io.length; e2--; )
    delete Co[No][Io[e2]];
  return Co();
};
Ao[_o] = true;
var ko = Object.create || function(t3, r2) {
  var n2;
  return null !== t3 ? (To[No] = Mo(t3), n2 = new To(), To[No] = null, n2[_o] = t3) : n2 = Co(), void 0 === r2 ? n2 : Do.f(n2, r2);
};
var Lo = m;
var Uo = g.RegExp;
var Wo = Lo(function() {
  var t3 = Uo(".", "s");
  return !(t3.dotAll && t3.exec("\n") && "s" === t3.flags);
});
var Ho = m;
var qo = g.RegExp;
var zo = Ho(function() {
  var t3 = qo("(?<a>b)", "g");
  return "b" !== t3.exec("b").groups.a || "bc" !== "b".replace(t3, "$<a>c");
});
var Go = O;
var Ko = Y;
var Bo = io;
var Jo = uo;
var Vo = ho;
var Xo = Tt.exports;
var Qo = ko;
var Zo = Hn.get;
var tu = Wo;
var ru = zo;
var nu = Xo("native-string-replace", String.prototype.replace);
var eu = RegExp.prototype.exec;
var iu = eu;
var ou = Ko("".charAt);
var uu = Ko("".indexOf);
var cu = Ko("".replace);
var au = Ko("".slice);
var fu = function() {
  var t3 = /a/, r2 = /b*/g;
  return Go(eu, t3, "a"), Go(eu, r2, "a"), 0 !== t3.lastIndex || 0 !== r2.lastIndex;
}();
var su = Vo.BROKEN_CARET;
var lu = void 0 !== /()??/.exec("")[1];
(fu || lu || su || tu || ru) && (iu = function(t3) {
  var r2, n2, e2, i2, o2, u2, c2, a2 = this, f2 = Zo(a2), s2 = Bo(t3), l2 = f2.raw;
  if (l2)
    return l2.lastIndex = a2.lastIndex, r2 = Go(iu, l2, s2), a2.lastIndex = l2.lastIndex, r2;
  var h2 = f2.groups, d2 = su && a2.sticky, v2 = Go(Jo, a2), p2 = a2.source, g2 = 0, y2 = s2;
  if (d2 && (v2 = cu(v2, "y", ""), -1 === uu(v2, "g") && (v2 += "g"), y2 = au(s2, a2.lastIndex), a2.lastIndex > 0 && (!a2.multiline || a2.multiline && "\n" !== ou(s2, a2.lastIndex - 1)) && (p2 = "(?: " + p2 + ")", y2 = " " + y2, g2++), n2 = new RegExp("^(?:" + p2 + ")", v2)), lu && (n2 = new RegExp("^" + p2 + "$(?!\\s)", v2)), fu && (e2 = a2.lastIndex), i2 = Go(eu, d2 ? n2 : a2, y2), d2 ? i2 ? (i2.input = au(i2.input, g2), i2[0] = au(i2[0], g2), i2.index = a2.lastIndex, a2.lastIndex += i2[0].length) : a2.lastIndex = 0 : fu && i2 && (a2.lastIndex = a2.global ? i2.index + i2[0].length : e2), lu && i2 && i2.length > 1 && Go(nu, i2[0], n2, function() {
    for (o2 = 1; o2 < arguments.length - 2; o2++)
      void 0 === arguments[o2] && (i2[o2] = void 0);
  }), i2 && h2)
    for (i2.groups = u2 = Qo(null), o2 = 0; o2 < h2.length; o2++)
      u2[(c2 = h2[o2])[0]] = i2[c2[1]];
  return i2;
});
var hu = iu;
fi({ target: "RegExp", proto: true, forced: /./.exec !== hu }, { exec: hu });
var du;
var vu;
var pu = fi;
var gu = O;
var yu = Z;
var mu = Wr;
var bu = io;
var wu = (du = false, (vu = /[ac]/).exec = function() {
  return du = true, /./.exec.apply(this, arguments);
}, true === vu.test("abc") && du);
var xu = /./.test;
pu({ target: "RegExp", proto: true, forced: !wu }, { test: function(t3) {
  var r2 = mu(this), n2 = bu(t3), e2 = r2.exec;
  if (!yu(e2))
    return gu(xu, r2, n2);
  var i2 = gu(e2, r2, n2);
  return null !== i2 && (mu(i2), true);
} });
var Su = Z;
var Ou = String;
var Eu = TypeError;
var $u = Y;
var Mu = Wr;
var Du = function(t3) {
  if ("object" == typeof t3 || Su(t3))
    return t3;
  throw Eu("Can't set " + Ou(t3) + " as a prototype");
};
var Iu = Object.setPrototypeOf || ("__proto__" in {} ? function() {
  var t3, r2 = false, n2 = {};
  try {
    (t3 = $u(Object.getOwnPropertyDescriptor(Object.prototype, "__proto__").set))(n2, []), r2 = n2 instanceof Array;
  } catch (t4) {
  }
  return function(n3, e2) {
    return Mu(n3), Du(e2), r2 ? t3(n3, e2) : n3.__proto__ = e2, n3;
  };
}() : void 0);
var Au = Z;
var ju = nt;
var Ru = Iu;
var Nu = function(t3, r2, n2) {
  var e2, i2;
  return Ru && Au(e2 = r2.constructor) && e2 !== n2 && ju(i2 = e2.prototype) && i2 !== n2.prototype && Ru(t3, i2), t3;
};
var Pu = nt;
var _u = L;
var Tu = sr("match");
var Yu = function(t3) {
  var r2;
  return Pu(t3) && (void 0 !== (r2 = t3[Tu]) ? !!r2 : "RegExp" == _u(t3));
};
var Fu = O;
var Cu = Bt;
var ku = ut;
var Lu = uo;
var Uu = RegExp.prototype;
var Wu = function(t3) {
  var r2 = t3.flags;
  return void 0 !== r2 || "flags" in Uu || Cu(t3, "flags") || !ku(Uu, t3) ? r2 : Fu(Lu, t3);
};
var Hu = Fr.f;
var qu = ot;
var zu = Fr;
var Gu = b;
var Ku = sr("species");
var Bu = b;
var Ju = g;
var Vu = Y;
var Xu = ri;
var Qu = Nu;
var Zu = nn;
var tc = ce.f;
var rc = ut;
var nc = Yu;
var ec = io;
var ic = Wu;
var oc = ho;
var uc = function(t3, r2, n2) {
  n2 in t3 || Hu(t3, n2, { configurable: true, get: function() {
    return r2[n2];
  }, set: function(t4) {
    r2[n2] = t4;
  } });
};
var cc = ue;
var ac = m;
var fc = Bt;
var sc = Hn.enforce;
var lc = function(t3) {
  var r2 = qu(t3), n2 = zu.f;
  Gu && r2 && !r2[Ku] && n2(r2, Ku, { configurable: true, get: function() {
    return this;
  } });
};
var hc = Wo;
var dc = zo;
var vc = sr("match");
var pc = Ju.RegExp;
var gc = pc.prototype;
var yc = Ju.SyntaxError;
var mc = Vu(gc.exec);
var bc = Vu("".charAt);
var wc = Vu("".replace);
var xc = Vu("".indexOf);
var Sc = Vu("".slice);
var Oc = /^\?<[^\s\d!#%&*+<=>@^][^\s!#%&*+<=>@^]*>/;
var Ec = /a/g;
var $c = /a/g;
var Mc = new pc(Ec) !== Ec;
var Dc = oc.MISSED_STICKY;
var Ic = oc.UNSUPPORTED_Y;
var Ac = Bu && (!Mc || Dc || hc || dc || ac(function() {
  return $c[vc] = false, pc(Ec) != Ec || pc($c) == $c || "/a/i" != pc(Ec, "i");
}));
if (Xu("RegExp", Ac)) {
  for (jc = function(t3, r2) {
    var n2, e2, i2, o2, u2, c2, a2 = rc(gc, this), f2 = nc(t3), s2 = void 0 === r2, l2 = [], h2 = t3;
    if (!a2 && f2 && s2 && t3.constructor === jc)
      return t3;
    if ((f2 || rc(gc, t3)) && (t3 = t3.source, s2 && (r2 = ic(h2))), t3 = void 0 === t3 ? "" : ec(t3), r2 = void 0 === r2 ? "" : ec(r2), h2 = t3, hc && "dotAll" in Ec && (e2 = !!r2 && xc(r2, "s") > -1) && (r2 = wc(r2, /s/g, "")), n2 = r2, Dc && "sticky" in Ec && (i2 = !!r2 && xc(r2, "y") > -1) && Ic && (r2 = wc(r2, /y/g, "")), dc && (o2 = function(t4) {
      for (var r3, n3 = t4.length, e3 = 0, i3 = "", o3 = [], u3 = {}, c3 = false, a3 = false, f3 = 0, s3 = ""; e3 <= n3; e3++) {
        if ("\\" === (r3 = bc(t4, e3)))
          r3 += bc(t4, ++e3);
        else if ("]" === r3)
          c3 = false;
        else if (!c3)
          switch (true) {
            case "[" === r3:
              c3 = true;
              break;
            case "(" === r3:
              mc(Oc, Sc(t4, e3 + 1)) && (e3 += 2, a3 = true), i3 += r3, f3++;
              continue;
            case (">" === r3 && a3):
              if ("" === s3 || fc(u3, s3))
                throw new yc("Invalid capture group name");
              u3[s3] = true, o3[o3.length] = [s3, f3], a3 = false, s3 = "";
              continue;
          }
        a3 ? s3 += r3 : i3 += r3;
      }
      return [i3, o3];
    }(t3), t3 = o2[0], l2 = o2[1]), u2 = Qu(pc(t3, r2), a2 ? this : gc, jc), (e2 || i2 || l2.length) && (c2 = sc(u2), e2 && (c2.dotAll = true, c2.raw = jc(function(t4) {
      for (var r3, n3 = t4.length, e3 = 0, i3 = "", o3 = false; e3 <= n3; e3++)
        "\\" !== (r3 = bc(t4, e3)) ? o3 || "." !== r3 ? ("[" === r3 ? o3 = true : "]" === r3 && (o3 = false), i3 += r3) : i3 += "[\\s\\S]" : i3 += r3 + bc(t4, ++e3);
      return i3;
    }(t3), n2)), i2 && (c2.sticky = true), l2.length && (c2.groups = l2)), t3 !== h2)
      try {
        Zu(u2, "source", "" === h2 ? "(?:)" : h2);
      } catch (t4) {
      }
    return u2;
  }, Rc = tc(pc), Nc = 0; Rc.length > Nc; )
    uc(jc, pc, Rc[Nc++]);
  gc.constructor = jc, jc.prototype = gc, cc(Ju, "RegExp", jc, { constructor: true });
}
var jc;
var Rc;
var Nc;
lc("RegExp");
var Pc = en.exports;
var _c = Fr;
var Tc = function(t3, r2, n2) {
  return n2.get && Pc(n2.get, r2, { getter: true }), n2.set && Pc(n2.set, r2, { setter: true }), _c.f(t3, r2, n2);
};
var Yc = b;
var Fc = Wo;
var Cc = L;
var kc = Tc;
var Lc = Hn.get;
var Uc = RegExp.prototype;
var Wc = TypeError;
Yc && Fc && kc(Uc, "dotAll", { configurable: true, get: function() {
  if (this !== Uc) {
    if ("RegExp" === Cc(this))
      return !!Lc(this).dotAll;
    throw Wc("Incompatible receiver, RegExp required");
  }
} });
var Hc = b;
var qc = ho.MISSED_STICKY;
var zc = L;
var Gc = Tc;
var Kc = Hn.get;
var Bc = RegExp.prototype;
var Jc = TypeError;
Hc && qc && Gc(Bc, "sticky", { configurable: true, get: function() {
  if (this !== Bc) {
    if ("RegExp" === zc(this))
      return !!Kc(this).sticky;
    throw Jc("Incompatible receiver, RegExp required");
  }
} });
var Vc = sn.PROPER;
var Xc = ue;
var Qc = Wr;
var Zc = io;
var ta = m;
var ra = Wu;
var na = "toString";
var ea = RegExp.prototype[na];
var ia = ta(function() {
  return "/a/b" != ea.call({ source: "a", flags: "b" });
});
var oa = Vc && ea.name != na;
(ia || oa) && Xc(RegExp.prototype, na, function() {
  var t3 = Qc(this);
  return "/" + Zc(t3.source) + "/" + Zc(ra(t3));
}, { unsafe: true });
var ua = TypeError;
var ca = xr;
var aa = Fr;
var fa = j;
var sa = function(t3, r2, n2) {
  var e2 = ca(r2);
  e2 in t3 ? aa.f(t3, e2, fa(0, n2)) : t3[e2] = n2;
};
var la = Y;
var ha = m;
var da = Z;
var va = ro;
var pa = yn;
var ga = function() {
};
var ya = [];
var ma = ot("Reflect", "construct");
var ba = /^\s*(?:class|function)\b/;
var wa = la(ba.exec);
var xa = !ba.exec(ga);
var Sa = function(t3) {
  if (!da(t3))
    return false;
  try {
    return ma(ga, ya, t3), true;
  } catch (t4) {
    return false;
  }
};
var Oa = function(t3) {
  if (!da(t3))
    return false;
  switch (va(t3)) {
    case "AsyncFunction":
    case "GeneratorFunction":
    case "AsyncGeneratorFunction":
      return false;
  }
  try {
    return xa || !!wa(ba, pa(t3));
  } catch (t4) {
    return true;
  }
};
Oa.sham = true;
var Ea = !ma || ha(function() {
  var t3;
  return Sa(Sa.call) || !Sa(Object) || !Sa(function() {
    t3 = true;
  }) || t3;
}) ? Oa : Sa;
var $a = gi;
var Ma = Ea;
var Da = nt;
var Ia = sr("species");
var Aa = Array;
var ja = function(t3) {
  var r2;
  return $a(t3) && (r2 = t3.constructor, (Ma(r2) && (r2 === Aa || $a(r2.prototype)) || Da(r2) && null === (r2 = r2[Ia])) && (r2 = void 0)), void 0 === r2 ? Aa : r2;
};
var Ra = m;
var Na = dt;
var Pa = sr("species");
var _a = function(t3) {
  return Na >= 51 || !Ra(function() {
    var r2 = [];
    return (r2.constructor = {})[Pa] = function() {
      return { foo: 1 };
    }, 1 !== r2[t3](Boolean).foo;
  });
};
var Ta = fi;
var Ya = m;
var Fa = gi;
var Ca = nt;
var ka = zt;
var La = we;
var Ua = function(t3) {
  if (t3 > 9007199254740991)
    throw ua("Maximum allowed index exceeded");
  return t3;
};
var Wa = sa;
var Ha = function(t3, r2) {
  return new (ja(t3))(0 === r2 ? 0 : r2);
};
var qa = _a;
var za = dt;
var Ga = sr("isConcatSpreadable");
var Ka = za >= 51 || !Ya(function() {
  var t3 = [];
  return t3[Ga] = false, t3.concat()[0] !== t3;
});
var Ba = qa("concat");
var Ja = function(t3) {
  if (!Ca(t3))
    return false;
  var r2 = t3[Ga];
  return void 0 !== r2 ? !!r2 : Fa(t3);
};
Ta({ target: "Array", proto: true, arity: 1, forced: !Ka || !Ba }, { concat: function(t3) {
  var r2, n2, e2, i2, o2, u2 = ka(this), c2 = Ha(u2, 0), a2 = 0;
  for (r2 = -1, e2 = arguments.length; r2 < e2; r2++)
    if (Ja(o2 = -1 === r2 ? u2 : arguments[r2]))
      for (i2 = La(o2), Ua(a2 + i2), n2 = 0; n2 < i2; n2++, a2++)
        n2 in o2 && Wa(c2, a2, o2[n2]);
    else
      Ua(a2 + 1), Wa(c2, a2++, o2);
  return c2.length = a2, c2;
} });
var Va = /^(1[3-9])\d{9}$/i;
var Xa = function(t3) {
  return Va.test(t3);
};
var Qa = function() {
  return new RegExp("^\\d{".concat(arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 6, "}$"));
};
var Za = function(t3) {
  return Qa(arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 6).test(t3);
};
var tf = /\D+/g;
var rf = function(t3) {
  return tf.test(t3);
};
var nf = /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{2})(\d)(\d|X|x)$/;
var ef = function(t3) {
  return nf.test(t3);
};
var of = /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/;
var uf = function(t3) {
  return of.test(t3);
};
var cf = /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/;
var af = function(t3) {
  return cf.test(t3);
};
var ff = /^(?:(http|https|ftp):\/\/)?((?:[\w]+\.)+[a-z0-9]+)((?:\/[^/?#]*)+)?(\?[^#]+)?(#.+)?$/i;
var sf = function(t3) {
  return ff.test(t3);
};
var lf = function() {
  var t3 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 17;
  return new RegExp("^[a-zA-Z]\\w{".concat(arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 5, ",").concat(t3, "}$"));
};
var hf = function(t3) {
  return lf(arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 5, arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 17).test(t3);
};
var df = function() {
  var t3 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return new RegExp("^\\d+(\\.\\d{".concat(arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1, ",").concat(t3, "})?$"));
};
var vf = function(t3) {
  return df(arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1, arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 2).test(t3);
};
var pf = function() {
  var t3 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return new RegExp("^[\\u4E00-\\u9FA5]{".concat(arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1, ",").concat(t3, "}$"));
};
var gf = function(t3) {
  return pf(arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1, arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 2).test(t3);
};
var yf = ro;
var mf = Bi ? {}.toString : function() {
  return "[object " + yf(this) + "]";
};
Bi || ue(Object.prototype, "toString", mf, { unsafe: true });
var bf = Y(1 .valueOf);
var wf = "	\n\v\f\r                　\u2028\u2029\uFEFF";
var xf = J;
var Sf = io;
var Of = wf;
var Ef = Y("".replace);
var $f = "[" + Of + "]";
var Mf = RegExp("^" + $f + $f + "*");
var Df = RegExp($f + $f + "*$");
var If = function(t3) {
  return function(r2) {
    var n2 = Sf(xf(r2));
    return 1 & t3 && (n2 = Ef(n2, Mf, "")), 2 & t3 && (n2 = Ef(n2, Df, "")), n2;
  };
};
var Af = { start: If(1), end: If(2), trim: If(3) };
var jf = b;
var Rf = g;
var Nf = Y;
var Pf = ri;
var _f = ue;
var Tf = Bt;
var Yf = Nu;
var Ff = ut;
var Cf = St;
var kf = mr;
var Lf = m;
var Uf = ce.f;
var Wf = y.f;
var Hf = Fr.f;
var qf = bf;
var zf = Af.trim;
var Gf = "Number";
var Kf = Rf[Gf];
var Bf = Kf.prototype;
var Jf = Rf.TypeError;
var Vf = Nf("".slice);
var Xf = Nf("".charCodeAt);
var Qf = function(t3) {
  var r2, n2, e2, i2, o2, u2, c2, a2, f2 = kf(t3, "number");
  if (Cf(f2))
    throw Jf("Cannot convert a Symbol value to a number");
  if ("string" == typeof f2 && f2.length > 2) {
    if (f2 = zf(f2), 43 === (r2 = Xf(f2, 0)) || 45 === r2) {
      if (88 === (n2 = Xf(f2, 2)) || 120 === n2)
        return NaN;
    } else if (48 === r2) {
      switch (Xf(f2, 1)) {
        case 66:
        case 98:
          e2 = 2, i2 = 49;
          break;
        case 79:
        case 111:
          e2 = 8, i2 = 55;
          break;
        default:
          return +f2;
      }
      for (u2 = (o2 = Vf(f2, 2)).length, c2 = 0; c2 < u2; c2++)
        if ((a2 = Xf(o2, c2)) < 48 || a2 > i2)
          return NaN;
      return parseInt(o2, e2);
    }
  }
  return +f2;
};
if (Pf(Gf, !Kf(" 0o1") || !Kf("0b1") || Kf("+0x1"))) {
  for (ts = function(t3) {
    var r2 = arguments.length < 1 ? 0 : Kf(function(t4) {
      var r3 = kf(t4, "number");
      return "bigint" == typeof r3 ? r3 : Qf(r3);
    }(t3)), n2 = this;
    return Ff(Bf, n2) && Lf(function() {
      qf(n2);
    }) ? Yf(Object(r2), n2, ts) : r2;
  }, rs = jf ? Uf(Kf) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,isFinite,isInteger,isNaN,isSafeInteger,parseFloat,parseInt,fromString,range".split(","), ns = 0; rs.length > ns; ns++)
    Tf(Kf, Zf = rs[ns]) && !Tf(ts, Zf) && Hf(ts, Zf, Wf(Kf, Zf));
  ts.prototype = Bf, Bf.constructor = ts, _f(Rf, Gf, ts, { constructor: true });
}
var Zf;
var ts;
var rs;
var ns;
var es = le;
var is = io;
var os = J;
var us = RangeError;
var cs = fi;
var as = Y;
var fs = le;
var ss = bf;
var ls = function(t3) {
  var r2 = is(os(this)), n2 = "", e2 = es(t3);
  if (e2 < 0 || e2 == 1 / 0)
    throw us("Wrong number of repetitions");
  for (; e2 > 0; (e2 >>>= 1) && (r2 += r2))
    1 & e2 && (n2 += r2);
  return n2;
};
var hs = m;
var ds = RangeError;
var vs = String;
var ps = Math.floor;
var gs = as(ls);
var ys = as("".slice);
var ms = as(1 .toFixed);
var bs = function(t3, r2, n2) {
  return 0 === r2 ? n2 : r2 % 2 == 1 ? bs(t3, r2 - 1, n2 * t3) : bs(t3 * t3, r2 / 2, n2);
};
var ws = function(t3, r2, n2) {
  for (var e2 = -1, i2 = n2; ++e2 < 6; )
    i2 += r2 * t3[e2], t3[e2] = i2 % 1e7, i2 = ps(i2 / 1e7);
};
var xs = function(t3, r2) {
  for (var n2 = 6, e2 = 0; --n2 >= 0; )
    e2 += t3[n2], t3[n2] = ps(e2 / r2), e2 = e2 % r2 * 1e7;
};
var Ss = function(t3) {
  for (var r2 = 6, n2 = ""; --r2 >= 0; )
    if ("" !== n2 || 0 === r2 || 0 !== t3[r2]) {
      var e2 = vs(t3[r2]);
      n2 = "" === n2 ? e2 : n2 + gs("0", 7 - e2.length) + e2;
    }
  return n2;
};
cs({ target: "Number", proto: true, forced: hs(function() {
  return "0.000" !== ms(8e-5, 3) || "1" !== ms(0.9, 0) || "1.25" !== ms(1.255, 2) || "1000000000000000128" !== ms(1000000000000000100, 0);
}) || !hs(function() {
  ms({});
}) }, { toFixed: function(t3) {
  var r2, n2, e2, i2, o2 = ss(this), u2 = fs(t3), c2 = [0, 0, 0, 0, 0, 0], a2 = "", f2 = "0";
  if (u2 < 0 || u2 > 20)
    throw ds("Incorrect fraction digits");
  if (o2 != o2)
    return "NaN";
  if (o2 <= -1e21 || o2 >= 1e21)
    return vs(o2);
  if (o2 < 0 && (a2 = "-", o2 = -o2), o2 > 1e-21)
    if (n2 = (r2 = function(t4) {
      for (var r3 = 0, n3 = t4; n3 >= 4096; )
        r3 += 12, n3 /= 4096;
      for (; n3 >= 2; )
        r3 += 1, n3 /= 2;
      return r3;
    }(o2 * bs(2, 69, 1)) - 69) < 0 ? o2 * bs(2, -r2, 1) : o2 / bs(2, r2, 1), n2 *= 4503599627370496, (r2 = 52 - r2) > 0) {
      for (ws(c2, 0, n2), e2 = u2; e2 >= 7; )
        ws(c2, 1e7, 0), e2 -= 7;
      for (ws(c2, bs(10, e2, 1), 0), e2 = r2 - 1; e2 >= 23; )
        xs(c2, 1 << 23), e2 -= 23;
      xs(c2, 1 << e2), ws(c2, 1, 1), xs(c2, 2), f2 = Ss(c2);
    } else
      ws(c2, 0, n2), ws(c2, 1 << -r2, 0), f2 = Ss(c2) + gs("0", u2);
  return f2 = u2 > 0 ? a2 + ((i2 = f2.length) <= u2 ? "0." + gs("0", u2 - i2) + f2 : ys(f2, 0, i2 - u2) + "." + ys(f2, i2 - u2)) : a2 + f2;
} });
var Os = m;
var Es = fi;
var $s = z;
var Ms = Q;
var Ds = function(t3, r2) {
  var n2 = [][t3];
  return !!n2 && Os(function() {
    n2.call(null, r2 || function() {
      return 1;
    }, 1);
  });
};
var Is = Y([].join);
var As = $s != Object;
var js = Ds("join", ",");
Es({ target: "Array", proto: true, forced: As || !js }, { join: function(t3) {
  return Is(Ms(this), void 0 === t3 ? "," : t3);
} });
var Rs = zt;
var Ns = pe;
var Ps = we;
var _s = sr;
var Ts = ko;
var Ys = Fr.f;
var Fs = _s("unscopables");
var Cs = Array.prototype;
null == Cs[Fs] && Ys(Cs, Fs, { configurable: true, value: Ts(null) });
var ks = function(t3) {
  Cs[Fs][t3] = true;
};
var Ls = function(t3) {
  for (var r2 = Rs(this), n2 = Ps(r2), e2 = arguments.length, i2 = Ns(e2 > 1 ? arguments[1] : void 0, n2), o2 = e2 > 2 ? arguments[2] : void 0, u2 = void 0 === o2 ? n2 : Ns(o2, n2); u2 > i2; )
    r2[i2++] = t3;
  return r2;
};
var Us = ks;
fi({ target: "Array", proto: true }, { fill: Ls }), Us("fill");
var Ws = 1e6;
var Hs = 1e6;
var qs = "[big.js] ";
var zs = qs + "Invalid ";
var Gs = zs + "decimal places";
var Ks = zs + "rounding mode";
var Bs = qs + "Division by zero";
var Js = {};
var Vs = void 0;
var Xs = /^-?(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i;
function Qs(t3, r2, n2, e2) {
  var i2 = t3.c;
  if (n2 === Vs && (n2 = t3.constructor.RM), 0 !== n2 && 1 !== n2 && 2 !== n2 && 3 !== n2)
    throw Error(Ks);
  if (r2 < 1)
    e2 = 3 === n2 && (e2 || !!i2[0]) || 0 === r2 && (1 === n2 && i2[0] >= 5 || 2 === n2 && (i2[0] > 5 || 5 === i2[0] && (e2 || i2[1] !== Vs))), i2.length = 1, e2 ? (t3.e = t3.e - r2 + 1, i2[0] = 1) : i2[0] = t3.e = 0;
  else if (r2 < i2.length) {
    if (e2 = 1 === n2 && i2[r2] >= 5 || 2 === n2 && (i2[r2] > 5 || 5 === i2[r2] && (e2 || i2[r2 + 1] !== Vs || 1 & i2[r2 - 1])) || 3 === n2 && (e2 || !!i2[0]), i2.length = r2, e2) {
      for (; ++i2[--r2] > 9; )
        if (i2[r2] = 0, 0 === r2) {
          ++t3.e, i2.unshift(1);
          break;
        }
    }
    for (r2 = i2.length; !i2[--r2]; )
      i2.pop();
  }
  return t3;
}
function Zs(t3, r2, n2) {
  var e2 = t3.e, i2 = t3.c.join(""), o2 = i2.length;
  if (r2)
    i2 = i2.charAt(0) + (o2 > 1 ? "." + i2.slice(1) : "") + (e2 < 0 ? "e" : "e+") + e2;
  else if (e2 < 0) {
    for (; ++e2; )
      i2 = "0" + i2;
    i2 = "0." + i2;
  } else if (e2 > 0)
    if (++e2 > o2)
      for (e2 -= o2; e2--; )
        i2 += "0";
    else
      e2 < o2 && (i2 = i2.slice(0, e2) + "." + i2.slice(e2));
  else
    o2 > 1 && (i2 = i2.charAt(0) + "." + i2.slice(1));
  return t3.s < 0 && n2 ? "-" + i2 : i2;
}
Js.abs = function() {
  var t3 = new this.constructor(this);
  return t3.s = 1, t3;
}, Js.cmp = function(t3) {
  var r2, n2 = this, e2 = n2.c, i2 = (t3 = new n2.constructor(t3)).c, o2 = n2.s, u2 = t3.s, c2 = n2.e, a2 = t3.e;
  if (!e2[0] || !i2[0])
    return e2[0] ? o2 : i2[0] ? -u2 : 0;
  if (o2 != u2)
    return o2;
  if (r2 = o2 < 0, c2 != a2)
    return c2 > a2 ^ r2 ? 1 : -1;
  for (u2 = (c2 = e2.length) < (a2 = i2.length) ? c2 : a2, o2 = -1; ++o2 < u2; )
    if (e2[o2] != i2[o2])
      return e2[o2] > i2[o2] ^ r2 ? 1 : -1;
  return c2 == a2 ? 0 : c2 > a2 ^ r2 ? 1 : -1;
}, Js.div = function(t3) {
  var r2 = this, n2 = r2.constructor, e2 = r2.c, i2 = (t3 = new n2(t3)).c, o2 = r2.s == t3.s ? 1 : -1, u2 = n2.DP;
  if (u2 !== ~~u2 || u2 < 0 || u2 > Ws)
    throw Error(Gs);
  if (!i2[0])
    throw Error(Bs);
  if (!e2[0])
    return t3.s = o2, t3.c = [t3.e = 0], t3;
  var c2, a2, f2, s2, l2, h2 = i2.slice(), d2 = c2 = i2.length, v2 = e2.length, p2 = e2.slice(0, c2), g2 = p2.length, y2 = t3, m2 = y2.c = [], b2 = 0, w2 = u2 + (y2.e = r2.e - t3.e) + 1;
  for (y2.s = o2, o2 = w2 < 0 ? 0 : w2, h2.unshift(0); g2++ < c2; )
    p2.push(0);
  do {
    for (f2 = 0; f2 < 10; f2++) {
      if (c2 != (g2 = p2.length))
        s2 = c2 > g2 ? 1 : -1;
      else
        for (l2 = -1, s2 = 0; ++l2 < c2; )
          if (i2[l2] != p2[l2]) {
            s2 = i2[l2] > p2[l2] ? 1 : -1;
            break;
          }
      if (!(s2 < 0))
        break;
      for (a2 = g2 == c2 ? i2 : h2; g2; ) {
        if (p2[--g2] < a2[g2]) {
          for (l2 = g2; l2 && !p2[--l2]; )
            p2[l2] = 9;
          --p2[l2], p2[g2] += 10;
        }
        p2[g2] -= a2[g2];
      }
      for (; !p2[0]; )
        p2.shift();
    }
    m2[b2++] = s2 ? f2 : ++f2, p2[0] && s2 ? p2[g2] = e2[d2] || 0 : p2 = [e2[d2]];
  } while ((d2++ < v2 || p2[0] !== Vs) && o2--);
  return m2[0] || 1 == b2 || (m2.shift(), y2.e--, w2--), b2 > w2 && Qs(y2, w2, n2.RM, p2[0] !== Vs), y2;
}, Js.eq = function(t3) {
  return 0 === this.cmp(t3);
}, Js.gt = function(t3) {
  return this.cmp(t3) > 0;
}, Js.gte = function(t3) {
  return this.cmp(t3) > -1;
}, Js.lt = function(t3) {
  return this.cmp(t3) < 0;
}, Js.lte = function(t3) {
  return this.cmp(t3) < 1;
}, Js.minus = Js.sub = function(t3) {
  var r2, n2, e2, i2, o2 = this, u2 = o2.constructor, c2 = o2.s, a2 = (t3 = new u2(t3)).s;
  if (c2 != a2)
    return t3.s = -a2, o2.plus(t3);
  var f2 = o2.c.slice(), s2 = o2.e, l2 = t3.c, h2 = t3.e;
  if (!f2[0] || !l2[0])
    return l2[0] ? t3.s = -a2 : f2[0] ? t3 = new u2(o2) : t3.s = 1, t3;
  if (c2 = s2 - h2) {
    for ((i2 = c2 < 0) ? (c2 = -c2, e2 = f2) : (h2 = s2, e2 = l2), e2.reverse(), a2 = c2; a2--; )
      e2.push(0);
    e2.reverse();
  } else
    for (n2 = ((i2 = f2.length < l2.length) ? f2 : l2).length, c2 = a2 = 0; a2 < n2; a2++)
      if (f2[a2] != l2[a2]) {
        i2 = f2[a2] < l2[a2];
        break;
      }
  if (i2 && (e2 = f2, f2 = l2, l2 = e2, t3.s = -t3.s), (a2 = (n2 = l2.length) - (r2 = f2.length)) > 0)
    for (; a2--; )
      f2[r2++] = 0;
  for (a2 = r2; n2 > c2; ) {
    if (f2[--n2] < l2[n2]) {
      for (r2 = n2; r2 && !f2[--r2]; )
        f2[r2] = 9;
      --f2[r2], f2[n2] += 10;
    }
    f2[n2] -= l2[n2];
  }
  for (; 0 === f2[--a2]; )
    f2.pop();
  for (; 0 === f2[0]; )
    f2.shift(), --h2;
  return f2[0] || (t3.s = 1, f2 = [h2 = 0]), t3.c = f2, t3.e = h2, t3;
}, Js.mod = function(t3) {
  var r2, n2 = this, e2 = n2.constructor, i2 = n2.s, o2 = (t3 = new e2(t3)).s;
  if (!t3.c[0])
    throw Error(Bs);
  return n2.s = t3.s = 1, r2 = 1 == t3.cmp(n2), n2.s = i2, t3.s = o2, r2 ? new e2(n2) : (i2 = e2.DP, o2 = e2.RM, e2.DP = e2.RM = 0, n2 = n2.div(t3), e2.DP = i2, e2.RM = o2, this.minus(n2.times(t3)));
}, Js.neg = function() {
  var t3 = new this.constructor(this);
  return t3.s = -t3.s, t3;
}, Js.plus = Js.add = function(t3) {
  var r2, n2, e2, i2 = this, o2 = i2.constructor;
  if (t3 = new o2(t3), i2.s != t3.s)
    return t3.s = -t3.s, i2.minus(t3);
  var u2 = i2.e, c2 = i2.c, a2 = t3.e, f2 = t3.c;
  if (!c2[0] || !f2[0])
    return f2[0] || (c2[0] ? t3 = new o2(i2) : t3.s = i2.s), t3;
  if (c2 = c2.slice(), r2 = u2 - a2) {
    for (r2 > 0 ? (a2 = u2, e2 = f2) : (r2 = -r2, e2 = c2), e2.reverse(); r2--; )
      e2.push(0);
    e2.reverse();
  }
  for (c2.length - f2.length < 0 && (e2 = f2, f2 = c2, c2 = e2), r2 = f2.length, n2 = 0; r2; c2[r2] %= 10)
    n2 = (c2[--r2] = c2[r2] + f2[r2] + n2) / 10 | 0;
  for (n2 && (c2.unshift(n2), ++a2), r2 = c2.length; 0 === c2[--r2]; )
    c2.pop();
  return t3.c = c2, t3.e = a2, t3;
}, Js.pow = function(t3) {
  var r2 = this, n2 = new r2.constructor("1"), e2 = n2, i2 = t3 < 0;
  if (t3 !== ~~t3 || t3 < -1e6 || t3 > Hs)
    throw Error(zs + "exponent");
  for (i2 && (t3 = -t3); 1 & t3 && (e2 = e2.times(r2)), t3 >>= 1; )
    r2 = r2.times(r2);
  return i2 ? n2.div(e2) : e2;
}, Js.prec = function(t3, r2) {
  if (t3 !== ~~t3 || t3 < 1 || t3 > Ws)
    throw Error(zs + "precision");
  return Qs(new this.constructor(this), t3, r2);
}, Js.round = function(t3, r2) {
  if (t3 === Vs)
    t3 = 0;
  else if (t3 !== ~~t3 || t3 < -Ws || t3 > Ws)
    throw Error(Gs);
  return Qs(new this.constructor(this), t3 + this.e + 1, r2);
}, Js.sqrt = function() {
  var t3, r2, n2, e2 = this, i2 = e2.constructor, o2 = e2.s, u2 = e2.e, c2 = new i2("0.5");
  if (!e2.c[0])
    return new i2(e2);
  if (o2 < 0)
    throw Error(qs + "No square root");
  0 === (o2 = Math.sqrt(e2 + "")) || o2 === 1 / 0 ? ((r2 = e2.c.join("")).length + u2 & 1 || (r2 += "0"), u2 = ((u2 + 1) / 2 | 0) - (u2 < 0 || 1 & u2), t3 = new i2(((o2 = Math.sqrt(r2)) == 1 / 0 ? "5e" : (o2 = o2.toExponential()).slice(0, o2.indexOf("e") + 1)) + u2)) : t3 = new i2(o2 + ""), u2 = t3.e + (i2.DP += 4);
  do {
    n2 = t3, t3 = c2.times(n2.plus(e2.div(n2)));
  } while (n2.c.slice(0, u2).join("") !== t3.c.slice(0, u2).join(""));
  return Qs(t3, (i2.DP -= 4) + t3.e + 1, i2.RM);
}, Js.times = Js.mul = function(t3) {
  var r2, n2 = this, e2 = n2.constructor, i2 = n2.c, o2 = (t3 = new e2(t3)).c, u2 = i2.length, c2 = o2.length, a2 = n2.e, f2 = t3.e;
  if (t3.s = n2.s == t3.s ? 1 : -1, !i2[0] || !o2[0])
    return t3.c = [t3.e = 0], t3;
  for (t3.e = a2 + f2, u2 < c2 && (r2 = i2, i2 = o2, o2 = r2, f2 = u2, u2 = c2, c2 = f2), r2 = new Array(f2 = u2 + c2); f2--; )
    r2[f2] = 0;
  for (a2 = c2; a2--; ) {
    for (c2 = 0, f2 = u2 + a2; f2 > a2; )
      c2 = r2[f2] + o2[a2] * i2[f2 - a2 - 1] + c2, r2[f2--] = c2 % 10, c2 = c2 / 10 | 0;
    r2[f2] = c2;
  }
  for (c2 ? ++t3.e : r2.shift(), a2 = r2.length; !r2[--a2]; )
    r2.pop();
  return t3.c = r2, t3;
}, Js.toExponential = function(t3, r2) {
  var n2 = this, e2 = n2.c[0];
  if (t3 !== Vs) {
    if (t3 !== ~~t3 || t3 < 0 || t3 > Ws)
      throw Error(Gs);
    for (n2 = Qs(new n2.constructor(n2), ++t3, r2); n2.c.length < t3; )
      n2.c.push(0);
  }
  return Zs(n2, true, !!e2);
}, Js.toFixed = function(t3, r2) {
  var n2 = this, e2 = n2.c[0];
  if (t3 !== Vs) {
    if (t3 !== ~~t3 || t3 < 0 || t3 > Ws)
      throw Error(Gs);
    for (t3 = t3 + (n2 = Qs(new n2.constructor(n2), t3 + n2.e + 1, r2)).e + 1; n2.c.length < t3; )
      n2.c.push(0);
  }
  return Zs(n2, false, !!e2);
}, Js[Symbol.for("nodejs.util.inspect.custom")] = Js.toJSON = Js.toString = function() {
  var t3 = this, r2 = t3.constructor;
  return Zs(t3, t3.e <= r2.NE || t3.e >= r2.PE, !!t3.c[0]);
}, Js.toNumber = function() {
  var t3 = Number(Zs(this, true, true));
  if (true === this.constructor.strict && !this.eq(t3.toString()))
    throw Error(qs + "Imprecise conversion");
  return t3;
}, Js.toPrecision = function(t3, r2) {
  var n2 = this, e2 = n2.constructor, i2 = n2.c[0];
  if (t3 !== Vs) {
    if (t3 !== ~~t3 || t3 < 1 || t3 > Ws)
      throw Error(zs + "precision");
    for (n2 = Qs(new e2(n2), t3, r2); n2.c.length < t3; )
      n2.c.push(0);
  }
  return Zs(n2, t3 <= n2.e || n2.e <= e2.NE || n2.e >= e2.PE, !!i2);
}, Js.valueOf = function() {
  var t3 = this, r2 = t3.constructor;
  if (true === r2.strict)
    throw Error(qs + "valueOf disallowed");
  return Zs(t3, t3.e <= r2.NE || t3.e >= r2.PE, true);
};
var tl = function t2() {
  function r2(n2) {
    var e2 = this;
    if (!(e2 instanceof r2))
      return n2 === Vs ? t2() : new r2(n2);
    if (n2 instanceof r2)
      e2.s = n2.s, e2.e = n2.e, e2.c = n2.c.slice();
    else {
      if ("string" != typeof n2) {
        if (true === r2.strict && "bigint" != typeof n2)
          throw TypeError(zs + "value");
        n2 = 0 === n2 && 1 / n2 < 0 ? "-0" : String(n2);
      }
      !function(t3, r3) {
        var n3, e3, i2;
        if (!Xs.test(r3))
          throw Error(zs + "number");
        t3.s = "-" == r3.charAt(0) ? (r3 = r3.slice(1), -1) : 1, (n3 = r3.indexOf(".")) > -1 && (r3 = r3.replace(".", ""));
        (e3 = r3.search(/e/i)) > 0 ? (n3 < 0 && (n3 = e3), n3 += +r3.slice(e3 + 1), r3 = r3.substring(0, e3)) : n3 < 0 && (n3 = r3.length);
        for (i2 = r3.length, e3 = 0; e3 < i2 && "0" == r3.charAt(e3); )
          ++e3;
        if (e3 == i2)
          t3.c = [t3.e = 0];
        else {
          for (; i2 > 0 && "0" == r3.charAt(--i2); )
            ;
          for (t3.e = n3 - e3 - 1, t3.c = [], n3 = 0; e3 <= i2; )
            t3.c[n3++] = +r3.charAt(e3++);
        }
      }(e2, n2);
    }
    e2.constructor = r2;
  }
  return r2.prototype = Js, r2.DP = 20, r2.RM = 1, r2.NE = -7, r2.PE = 21, r2.strict = false, r2.roundDown = 0, r2.roundHalfUp = 1, r2.roundHalfEven = 2, r2.roundUp = 3, r2;
}();
var rl = function(t3, r2) {
  return new tl(t3).plus(new tl(r2)).toString();
};
var nl = function(t3, r2) {
  return new tl(t3).minus(new tl(r2)).toString();
};
var el = function(t3, r2) {
  return new tl(t3).times(new tl(r2)).toString();
};
var il = function(t3, r2) {
  return 0 === Number(t3) && 0 === Number(r2) ? 0 : new tl(t3).div(new tl(r2)).toString();
};
var ol = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return new tl(t3).round(r2).toString();
};
var ul = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return Number(il(t3, 100)).toFixed(r2);
};
var cl = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return Number(el(t3, 100)).toFixed(r2);
};
var al = function(t3) {
  var r2 = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
  return t3 ? Number(il(t3, 1e4)).toFixed(r2) : r2 > 0 ? "0." + new Array(r2).fill(0).join("") : "0";
};
var fl = Y;
var sl = ue;
var ll = hu;
var hl = m;
var dl = sr;
var vl = nn;
var pl = dl("species");
var gl = RegExp.prototype;
var yl = function(t3, r2, n2, e2) {
  var i2 = dl(t3), o2 = !hl(function() {
    var r3 = {};
    return r3[i2] = function() {
      return 7;
    }, 7 != ""[t3](r3);
  }), u2 = o2 && !hl(function() {
    var r3 = false, n3 = /a/;
    return "split" === t3 && ((n3 = {}).constructor = {}, n3.constructor[pl] = function() {
      return n3;
    }, n3.flags = "", n3[i2] = /./[i2]), n3.exec = function() {
      return r3 = true, null;
    }, n3[i2](""), !r3;
  });
  if (!o2 || !u2 || n2) {
    var c2 = fl(/./[i2]), a2 = r2(i2, ""[t3], function(t4, r3, n3, e3, i3) {
      var u3 = fl(t4), a3 = r3.exec;
      return a3 === ll || a3 === gl.exec ? o2 && !i3 ? { done: true, value: c2(r3, n3, e3) } : { done: true, value: u3(n3, r3, e3) } : { done: false };
    });
    sl(String.prototype, t3, a2[0]), sl(gl, i2, a2[1]);
  }
  e2 && vl(gl[i2], "sham", true);
};
var ml = Ea;
var bl = Et;
var wl = TypeError;
var xl = Wr;
var Sl = function(t3) {
  if (ml(t3))
    return t3;
  throw wl(bl(t3) + " is not a constructor");
};
var Ol = G;
var El = sr("species");
var $l = Y;
var Ml = le;
var Dl = io;
var Il = J;
var Al = $l("".charAt);
var jl = $l("".charCodeAt);
var Rl = $l("".slice);
var Nl = function(t3) {
  return function(r2, n2) {
    var e2, i2, o2 = Dl(Il(r2)), u2 = Ml(n2), c2 = o2.length;
    return u2 < 0 || u2 >= c2 ? t3 ? "" : void 0 : (e2 = jl(o2, u2)) < 55296 || e2 > 56319 || u2 + 1 === c2 || (i2 = jl(o2, u2 + 1)) < 56320 || i2 > 57343 ? t3 ? Al(o2, u2) : e2 : t3 ? Rl(o2, u2, u2 + 2) : i2 - 56320 + (e2 - 55296 << 10) + 65536;
  };
};
var Pl = { codeAt: Nl(false), charAt: Nl(true) }.charAt;
var _l = function(t3, r2, n2) {
  return r2 + (n2 ? Pl(t3, r2).length : 1);
};
var Tl = pe;
var Yl = we;
var Fl = sa;
var Cl = Array;
var kl = Math.max;
var Ll = O;
var Ul = Wr;
var Wl = Z;
var Hl = L;
var ql = hu;
var zl = TypeError;
var Gl = function(t3, r2) {
  var n2 = t3.exec;
  if (Wl(n2)) {
    var e2 = Ll(n2, t3, r2);
    return null !== e2 && Ul(e2), e2;
  }
  if ("RegExp" === Hl(t3))
    return Ll(ql, t3, r2);
  throw zl("RegExp#exec called on incompatible receiver");
};
var Kl = vi;
var Bl = O;
var Jl = Y;
var Vl = yl;
var Xl = Wr;
var Ql = G;
var Zl = Yu;
var th = J;
var rh = function(t3, r2) {
  var n2, e2 = xl(t3).constructor;
  return void 0 === e2 || Ol(n2 = xl(e2)[El]) ? r2 : Sl(n2);
};
var nh = _l;
var eh = me;
var ih = io;
var oh = jt;
var uh = function(t3, r2, n2) {
  for (var e2 = Yl(t3), i2 = Tl(r2, e2), o2 = Tl(void 0 === n2 ? e2 : n2, e2), u2 = Cl(kl(o2 - i2, 0)), c2 = 0; i2 < o2; i2++, c2++)
    Fl(u2, c2, t3[i2]);
  return u2.length = c2, u2;
};
var ch = Gl;
var ah = hu;
var fh = m;
var sh = ho.UNSUPPORTED_Y;
var lh = 4294967295;
var hh = Math.min;
var dh = [].push;
var vh = Jl(/./.exec);
var ph = Jl(dh);
var gh = Jl("".slice);
var yh = !fh(function() {
  var t3 = /(?:)/, r2 = t3.exec;
  t3.exec = function() {
    return r2.apply(this, arguments);
  };
  var n2 = "ab".split(t3);
  return 2 !== n2.length || "a" !== n2[0] || "b" !== n2[1];
});
Vl("split", function(t3, r2, n2) {
  var e2;
  return e2 = "c" == "abbc".split(/(b)*/)[1] || 4 != "test".split(/(?:)/, -1).length || 2 != "ab".split(/(?:ab)*/).length || 4 != ".".split(/(.?)(.?)/).length || ".".split(/()()/).length > 1 || "".split(/.?/).length ? function(t4, n3) {
    var e3 = ih(th(this)), i2 = void 0 === n3 ? lh : n3 >>> 0;
    if (0 === i2)
      return [];
    if (void 0 === t4)
      return [e3];
    if (!Zl(t4))
      return Bl(r2, e3, t4, i2);
    for (var o2, u2, c2, a2 = [], f2 = (t4.ignoreCase ? "i" : "") + (t4.multiline ? "m" : "") + (t4.unicode ? "u" : "") + (t4.sticky ? "y" : ""), s2 = 0, l2 = new RegExp(t4.source, f2 + "g"); (o2 = Bl(ah, l2, e3)) && !((u2 = l2.lastIndex) > s2 && (ph(a2, gh(e3, s2, o2.index)), o2.length > 1 && o2.index < e3.length && Kl(dh, a2, uh(o2, 1)), c2 = o2[0].length, s2 = u2, a2.length >= i2)); )
      l2.lastIndex === o2.index && l2.lastIndex++;
    return s2 === e3.length ? !c2 && vh(l2, "") || ph(a2, "") : ph(a2, gh(e3, s2)), a2.length > i2 ? uh(a2, 0, i2) : a2;
  } : "0".split(void 0, 0).length ? function(t4, n3) {
    return void 0 === t4 && 0 === n3 ? [] : Bl(r2, this, t4, n3);
  } : r2, [function(r3, n3) {
    var i2 = th(this), o2 = Ql(r3) ? void 0 : oh(r3, t3);
    return o2 ? Bl(o2, r3, i2, n3) : Bl(e2, ih(i2), r3, n3);
  }, function(t4, i2) {
    var o2 = Xl(this), u2 = ih(t4), c2 = n2(e2, o2, u2, i2, e2 !== r2);
    if (c2.done)
      return c2.value;
    var a2 = rh(o2, RegExp), f2 = o2.unicode, s2 = (o2.ignoreCase ? "i" : "") + (o2.multiline ? "m" : "") + (o2.unicode ? "u" : "") + (sh ? "g" : "y"), l2 = new a2(sh ? "^(?:" + o2.source + ")" : o2, s2), h2 = void 0 === i2 ? lh : i2 >>> 0;
    if (0 === h2)
      return [];
    if (0 === u2.length)
      return null === ch(l2, u2) ? [u2] : [];
    for (var d2 = 0, v2 = 0, p2 = []; v2 < u2.length; ) {
      l2.lastIndex = sh ? 0 : v2;
      var g2, y2 = ch(l2, sh ? gh(u2, v2) : u2);
      if (null === y2 || (g2 = hh(eh(l2.lastIndex + (sh ? v2 : 0)), u2.length)) === d2)
        v2 = nh(u2, v2, f2);
      else {
        if (ph(p2, gh(u2, d2, v2)), p2.length === h2)
          return p2;
        for (var m2 = 1; m2 <= y2.length - 1; m2++)
          if (ph(p2, y2[m2]), p2.length === h2)
            return p2;
        v2 = d2 = g2;
      }
    }
    return ph(p2, gh(u2, d2)), p2;
  }];
}, !yh, sh);
var mh = g;
var bh = m;
var wh = io;
var xh = Af.trim;
var Sh = wf;
var Oh = Y("".charAt);
var Eh = mh.parseFloat;
var $h = mh.Symbol;
var Mh = $h && $h.iterator;
var Dh = 1 / Eh(Sh + "-0") != -1 / 0 || Mh && !bh(function() {
  Eh(Object(Mh));
}) ? function(t3) {
  var r2 = xh(wh(t3)), n2 = Eh(r2);
  return 0 === n2 && "-" == Oh(r2, 0) ? -0 : n2;
} : Eh;
fi({ global: true, forced: parseFloat != Dh }, { parseFloat: Dh });
var Ih = Y;
var Ah = zt;
var jh = Math.floor;
var Rh = Ih("".charAt);
var Nh = Ih("".replace);
var Ph = Ih("".slice);
var _h = /\$([$&'`]|\d{1,2}|<[^>]*>)/g;
var Th = /\$([$&'`]|\d{1,2})/g;
var Yh = vi;
var Fh = O;
var Ch = Y;
var kh = yl;
var Lh = m;
var Uh = Wr;
var Wh = Z;
var Hh = G;
var qh = le;
var zh = me;
var Gh = io;
var Kh = J;
var Bh = _l;
var Jh = jt;
var Vh = function(t3, r2, n2, e2, i2, o2) {
  var u2 = n2 + t3.length, c2 = e2.length, a2 = Th;
  return void 0 !== i2 && (i2 = Ah(i2), a2 = _h), Nh(o2, a2, function(o3, a3) {
    var f2;
    switch (Rh(a3, 0)) {
      case "$":
        return "$";
      case "&":
        return t3;
      case "`":
        return Ph(r2, 0, n2);
      case "'":
        return Ph(r2, u2);
      case "<":
        f2 = i2[Ph(a3, 1, -1)];
        break;
      default:
        var s2 = +a3;
        if (0 === s2)
          return o3;
        if (s2 > c2) {
          var l2 = jh(s2 / 10);
          return 0 === l2 ? o3 : l2 <= c2 ? void 0 === e2[l2 - 1] ? Rh(a3, 1) : e2[l2 - 1] + Rh(a3, 1) : o3;
        }
        f2 = e2[s2 - 1];
    }
    return void 0 === f2 ? "" : f2;
  });
};
var Xh = Gl;
var Qh = sr("replace");
var Zh = Math.max;
var td = Math.min;
var rd = Ch([].concat);
var nd = Ch([].push);
var ed = Ch("".indexOf);
var id = Ch("".slice);
var od = "$0" === "a".replace(/./, "$0");
var ud = !!/./[Qh] && "" === /./[Qh]("a", "$0");
var cd = !Lh(function() {
  var t3 = /./;
  return t3.exec = function() {
    var t4 = [];
    return t4.groups = { a: "7" }, t4;
  }, "7" !== "".replace(t3, "$<a>");
});
kh("replace", function(t3, r2, n2) {
  var e2 = ud ? "$" : "$0";
  return [function(t4, n3) {
    var e3 = Kh(this), i2 = Hh(t4) ? void 0 : Jh(t4, Qh);
    return i2 ? Fh(i2, t4, e3, n3) : Fh(r2, Gh(e3), t4, n3);
  }, function(t4, i2) {
    var o2 = Uh(this), u2 = Gh(t4);
    if ("string" == typeof i2 && -1 === ed(i2, e2) && -1 === ed(i2, "$<")) {
      var c2 = n2(r2, o2, u2, i2);
      if (c2.done)
        return c2.value;
    }
    var a2 = Wh(i2);
    a2 || (i2 = Gh(i2));
    var f2 = o2.global;
    if (f2) {
      var s2 = o2.unicode;
      o2.lastIndex = 0;
    }
    for (var l2 = []; ; ) {
      var h2 = Xh(o2, u2);
      if (null === h2)
        break;
      if (nd(l2, h2), !f2)
        break;
      "" === Gh(h2[0]) && (o2.lastIndex = Bh(u2, zh(o2.lastIndex), s2));
    }
    for (var d2, v2 = "", p2 = 0, g2 = 0; g2 < l2.length; g2++) {
      for (var y2 = Gh((h2 = l2[g2])[0]), m2 = Zh(td(qh(h2.index), u2.length), 0), b2 = [], w2 = 1; w2 < h2.length; w2++)
        nd(b2, void 0 === (d2 = h2[w2]) ? d2 : String(d2));
      var x2 = h2.groups;
      if (a2) {
        var S2 = rd([y2], b2, m2, u2);
        void 0 !== x2 && nd(S2, x2);
        var O2 = Gh(Yh(i2, void 0, S2));
      } else
        O2 = Vh(y2, u2, m2, b2, x2, i2);
      m2 >= p2 && (v2 += id(u2, p2, m2) + O2, p2 = m2 + y2.length);
    }
    return v2 + id(u2, p2);
  }];
}, !cd || !od || ud);
var ad = sn.PROPER;
var fd = m;
var sd = wf;
var ld = Af.trim;
fi({ target: "String", proto: true, forced: function(t3) {
  return fd(function() {
    return !!sd[t3]() || "​᠎" !== "​᠎"[t3]() || ad && sd[t3].name !== t3;
  });
}("trim") }, { trim: function() {
  return ld(this);
} });
var hd = fi;
var dd = gi;
var vd = Ea;
var pd = nt;
var gd = pe;
var yd = we;
var md = Q;
var bd = sa;
var wd = sr;
var xd = yi;
var Sd = _a("slice");
var Od = wd("species");
var Ed = Array;
var $d = Math.max;
hd({ target: "Array", proto: true, forced: !Sd }, { slice: function(t3, r2) {
  var n2, e2, i2, o2 = md(this), u2 = yd(o2), c2 = gd(t3, u2), a2 = gd(void 0 === r2 ? u2 : r2, u2);
  if (dd(o2) && (n2 = o2.constructor, (vd(n2) && (n2 === Ed || dd(n2.prototype)) || pd(n2) && null === (n2 = n2[Od])) && (n2 = void 0), n2 === Ed || void 0 === n2))
    return xd(o2, c2, a2);
  for (e2 = new (void 0 === n2 ? Ed : n2)($d(a2 - c2, 0)), i2 = 0; c2 < a2; c2++, i2++)
    c2 in o2 && bd(e2, i2, o2[c2]);
  return e2.length = i2, e2;
} });
var Md = $e.includes;
var Dd = ks;
fi({ target: "Array", proto: true, forced: m(function() {
  return !Array(1).includes();
}) }, { includes: function(t3) {
  return Md(this, t3, arguments.length > 1 ? arguments[1] : void 0);
} }), Dd("includes");
var Id = O;
var Ad = Wr;
var jd = G;
var Rd = me;
var Nd = io;
var Pd = J;
var _d = jt;
var Td = _l;
var Yd = Gl;
yl("match", function(t3, r2, n2) {
  return [function(r3) {
    var n3 = Pd(this), e2 = jd(r3) ? void 0 : _d(r3, t3);
    return e2 ? Id(e2, r3, n3) : new RegExp(r3)[t3](Nd(n3));
  }, function(t4) {
    var e2 = Ad(this), i2 = Nd(t4), o2 = n2(r2, e2, i2);
    if (o2.done)
      return o2.value;
    if (!e2.global)
      return Yd(e2, i2);
    var u2 = e2.unicode;
    e2.lastIndex = 0;
    for (var c2, a2 = [], f2 = 0; null !== (c2 = Yd(e2, i2)); ) {
      var s2 = Nd(c2[0]);
      a2[f2] = s2, "" === s2 && (e2.lastIndex = Td(i2, Rd(e2.lastIndex), u2)), f2++;
    }
    return 0 === f2 ? null : a2;
  }];
});
var Fd = function() {
  var t3 = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, r2 = t3.num, n2 = t3.decimals, e2 = void 0 === n2 ? 2 : n2, i2 = t3.decimal, o2 = void 0 === i2 ? "." : i2, u2 = t3.separator, c2 = void 0 === u2 ? "," : u2, a2 = t3.prefix, f2 = void 0 === a2 ? "" : a2, s2 = t3.suffix, l2 = void 0 === s2 ? "" : s2;
  if (r2 <= 0)
    return e2 > 0 ? "0." + new Array(e2).fill(0).join("") : "0";
  r2 = Number(r2).toFixed(e2);
  var h2 = (r2 += "").split("."), d2 = h2[0], v2 = h2.length > 1 ? o2 + h2[1] : "", p2 = /(\d+)(\d{3})/;
  if (c2 && isNaN(parseFloat(c2)))
    for (; p2.test(d2); )
      d2 = d2.replace(p2, "$1" + c2 + "$2");
  return f2 + d2 + v2 + l2;
};
var Cd = function(t3) {
  var r2, n2 = t3.replace(/\s|\D/gi, "");
  return null === (r2 = /(\d{3})?(\d{1,4})?(\d{1,4})?/.exec(n2)) || void 0 === r2 ? void 0 : r2.slice(1, 4).join(" ").trim();
};
var kd = function(t3) {
  var r2, n2 = t3.replace(/\s|\D/gi, ""), e2 = null === (r2 = /(\d{6})?(\d{1,8})?(\d{1,4})?/.exec(n2)) || void 0 === r2 ? void 0 : r2.slice(1, 4).join(" ").trim(), i2 = t3[19];
  return ["x", "X"].includes(i2) ? "".concat(e2).concat(i2) : e2;
};
var Ld = function(t3) {
  var r2, n2 = t3.replace(/\s|\D/gi, "");
  return null === (r2 = /(\d{4})?(\d{1,4})?(\d{1,4})?(\d{1,4})?(\d{1,4})?/.exec(n2)) || void 0 === r2 ? void 0 : r2.slice(1, 6).join(" ").trim();
};
var Ud = function(t3, r2) {
  var n2 = document.createElement("script");
  n2.onload = function() {
    r2 && r2();
  }, n2.src = t3, document.body.appendChild(n2);
};
var Wd = function() {
  var t3 = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : location.href, r2 = {};
  return t3.replace(/([^?=&#]+)=([^?=&#]+)/g, function(t4) {
    var n2 = t4.split("=");
    r2[n2[0]] = n2[1];
  }), r2;
};
var Hd = function(t3) {
  return t3.replace(tf, "");
};
var qd = function() {
  var t3 = navigator.userAgent;
  return { trident: t3.indexOf("Trident") > -1, presto: t3.indexOf("Presto") > -1, webKit: t3.indexOf("AppleWebKit") > -1, gecko: t3.indexOf("Gecko") > -1 && -1 === t3.indexOf("KHTML"), mobile: !!t3.match(/AppleWebKit.*Mobile.*/), ios: !!t3.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), android: t3.indexOf("Android") > -1 || t3.indexOf("Linux") > -1, iPhone: t3.indexOf("iPhone") > -1, iPad: t3.indexOf("iPad") > -1, webApp: -1 === t3.indexOf("Safari"), qq: !!t3.match(/QQ\/[0-9]/i), qqBrowser: t3.indexOf("QQBrowser") > -1, ali: t3.indexOf("AlipayClient") > -1, weixin: t3.indexOf("MicroMessenger") > -1, yys: t3.indexOf("UnionPay") > -1 };
};
var zd = function(t3) {
  var r2 = t3.split(/[^\d]+/);
  return "#" + (r2[1] << 16 | r2[2] << 8 | r2[3]).toString(16);
};
var Gd = function(t3) {
  var r2 = t3.replace("#", "0x");
  return "rgb(" + (r2 >> 16) + "," + (r2 >> 8 & 255) + "," + (255 & r2) + ")";
};
export {
  Ld as bankCardFormat,
  il as bigDivide,
  ul as bigFenToYuan,
  el as bigMultiply,
  rl as bigPlus,
  ol as bigRound,
  nl as bigSubtract,
  cl as bigYuanToFen,
  al as bigYuanTowan,
  kd as cardFormat,
  Gd as colorHexToRGB,
  zd as colorRGBToHex,
  o as dateAddDay,
  c as dateAddMonth,
  f as dateAddYears,
  d as dateDiffDay,
  v as dateDiffHour,
  h as dateDiffMonth,
  l as dateDiffYears,
  e as dateFormat,
  u as dateSubtractDay,
  a as dateSubtractMonth,
  s as dateSubtractYears,
  i as dateToTime,
  qd as getBrowser,
  ef as isCard,
  gf as isChinese,
  uf as isEmail,
  Xa as isMobile,
  vf as isMoney,
  rf as isNumber,
  af as isPhone,
  hf as isPwd,
  Za as isSMs,
  sf as isURl,
  Ud as loadScript,
  Cd as mobileFormat,
  Hd as noNumericToBlank,
  nf as patternCard,
  pf as patternChinese,
  of as patternEmail,
  Va as patternMobile,
  df as patternMoney,
  tf as patternNumber,
  cf as patternPhone,
  lf as patternPwd,
  Qa as patternSMs,
  ff as patternURl,
  Wd as queryUrlParams,
  Gi as storageDeleteKey,
  qi as storageGet,
  zi as storageSet,
  Fd as thousandths
};
/*! Bundled license information:

@fe-hl/shared/lib/index.esm.js:
  (*!
   * @fe-hl/shared v1.0.0
   * (c) 2022-2023 hl
   * Released under the MIT License.
   *)
*/
//# sourceMappingURL=@fe-hl_shared.js.map
